function findFancyNode(selector, id) {
    var newNode = $(selector).fancytree("getTree").findFirst(function (node) {
        return node.key === id
    });
    return newNode
}

function getDatatableEditor(options) {
    return new $.fn.dataTable.Editor(options)
}


