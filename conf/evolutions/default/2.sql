# --- !Ups

CREATE TABLE "public".employees (
  id          BIGSERIAL    NOT NULL,
  full_name   VARCHAR(255) NOT NULL,
  address     VARCHAR(255) NULL,
  email       VARCHAR(255) NULL,
  phone       VARCHAR(50)  NULL,
  position    VARCHAR(255) NULL,
  card_number VARCHAR(22)  NULL,
  user_id     int8,
  PRIMARY KEY (id)
);

CREATE INDEX employees_full_name
  ON "public".employees (full_name);
CREATE INDEX employees_user_id
  ON "public".employees (user_id);
ALTER TABLE "public".employees ADD CONSTRAINT FKemployees700133 FOREIGN KEY (user_id) REFERENCES "public".users (id) ON DELETE SET NULL;

INSERT INTO "public"."employees" (full_name, address, email, phone, position, card_number, user_id) VALUES ('Иванов Петр', null, null, null, null, null, '2');
INSERT INTO "public"."employees" (full_name, address, email, phone, position, card_number, user_id) VALUES ('Сидоров Юрий', null, null, null, null, null, null);

# --- !Downs
ALTER TABLE "public".employees DROP CONSTRAINT FKemployees700133;

DROP TABLE IF EXISTS "public".employees CASCADE;

