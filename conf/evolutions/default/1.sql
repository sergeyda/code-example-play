# --- !Ups

CREATE TABLE public.roles (
  id   bigserial NOT NULL,
  name varchar(100) NOT NULL,
  PRIMARY KEY (id));
CREATE UNIQUE INDEX roles_name
  ON "public"."roles" (name);


CREATE TABLE public.users (
  "id"                bigserial NOT NULL,
  login             varchar(150) NOT NULL,
  name              varchar(255) NOT NULL,
  password_hash      varchar(255) NOT NULL,
  "session"         varchar(255),
  role_id int8 NOT NULL,
  PRIMARY KEY ("id"));
CREATE UNIQUE INDEX users_login
  ON "public"."users" (login);
CREATE UNIQUE INDEX users_session
  ON "public"."users" ("session");
ALTER TABLE "public"."users" ADD CONSTRAINT "FKpublic.use299489" FOREIGN KEY (role_id) REFERENCES "public"."roles" (id);


CREATE TABLE "public".logs (
  id     bigserial NOT NULL,
  type   varchar(100) NOT NULL,
  text   varchar(255) NOT NULL,
  hidden bool DEFAULT 'false' NOT NULL,
  user_id int8 NOT NULL,
  time   timestamp(6) NOT NULL,
  PRIMARY KEY (id));
CREATE INDEX logs_text
  ON "public".logs (text);
CREATE INDEX logs_type
  ON "public".logs (type);
CREATE INDEX logs_time
  ON "public".logs (time);
ALTER TABLE "public".logs ADD CONSTRAINT FKlogs122233 FOREIGN KEY (user_id) REFERENCES "public".users (id);


INSERT INTO "public"."roles" (name) VALUES ('Admin');
INSERT INTO "public"."roles" (name) VALUES ('Cashier');
INSERT INTO "public"."users" (login, name, password_hash, role_id) VALUES ('tempadmin', 'Временный админ', 'un8q4+GiSxl24XOd7CDH57xxPNx07kCx9QheD/MK/0Q=$IWRf5pj0aCeCVIvD4xWdlZVp9DSCvo/fa13yPrVapp0=', 1);
INSERT INTO "public"."users" (login, name, password_hash, role_id) VALUES ('kassir1', 'Кассир 1', 'un8q4+GiSxl24XOd7CDH57xxPNx07kCx9QheD/MK/0Q=$IWRf5pj0aCeCVIvD4xWdlZVp9DSCvo/fa13yPrVapp0=', 2);


# --- !Downs

ALTER TABLE "public".logs DROP CONSTRAINT FKlogs122233;
DROP TABLE IF EXISTS "public".logs CASCADE;

DROP TABLE IF EXISTS "public"."users" CASCADE;
DROP TABLE IF EXISTS "public"."roles" CASCADE;

