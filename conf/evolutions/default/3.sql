# --- !Ups

CREATE TABLE "public".units (
  id    BIGSERIAL   NOT NULL,
  title VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
);
CREATE UNIQUE INDEX units_title
  ON "public".units (title);

CREATE TABLE "public".goods (
  id        BIGSERIAL    NOT NULL,
  title     VARCHAR(255) NOT NULL,
  parent_id INT8,
  unit_id   INT8,
  barcode   VARCHAR(255),
  code      VARCHAR(50),
  folder    BOOL         NOT NULL,
  PRIMARY KEY (id)
);
CREATE INDEX goods_title
  ON "public".goods (title);
CREATE INDEX goods_barcode
  ON "public".goods (barcode);
CREATE INDEX goods_code
  ON "public".goods (code);

ALTER TABLE "public".goods
  ADD CONSTRAINT FKgoods550775 FOREIGN KEY (unit_id) REFERENCES "public".units (id) ON DELETE RESTRICT;

INSERT INTO "public".units (title) VALUES ('шт');
# --- !Downs

ALTER TABLE "public".goods
  DROP CONSTRAINT FKgoods550775;
DROP TABLE IF EXISTS "public".goods CASCADE;
DROP TABLE IF EXISTS "public".units CASCADE;




