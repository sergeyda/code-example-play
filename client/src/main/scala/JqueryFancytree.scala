import org.scalajs.jquery.JQuery

import scala.scalajs.js
import scala.scalajs.js.Dynamic

/**
  * Created by sergey on 6/25/17.
  */
@js.native
trait JqueryFancytree extends JQuery {
  def fancytree(): JqueryFancytree = js.native
  def fancytree(init: Dynamic): JqueryFancytree = js.native
  def addChildren(init: Dynamic): JqueryFancytree = js.native
  def getSelectedNodes(): JqueryFancytree = js.native
  def findFirst(filter: (JqueryFancytree) => Boolean): JqueryFancytree = js.native
  def key: String = js.native
  def clearFilter(): JqueryFancytree = js.native
  def hasChildren(): Boolean = js.native
  def folder: Boolean = js.native
  def getParent(): JqueryFancytree = js.native
  def isFolder(): JqueryFancytree = js.native
  def renderStatus(): JqueryFancytree = js.native
  def fancytree(string: String): JqueryFancytree = js.native
  def filterNodes(string: String): JqueryFancytree = js.native
  def filterNodes(string: String, options: Dynamic): JqueryFancytree = js.native
  def editCreateNode(typeOf: String, title: String) = js.native
}


