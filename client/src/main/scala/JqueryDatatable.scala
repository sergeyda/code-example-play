import scala.scalajs.js
import org.scalajs.jquery.JQuery
import scala.scalajs.js.Dynamic

/**
  * Created by sergey on 6/25/17.
  */
@js.native
trait JqueryDatatable extends JQuery {
  var row: JqueryDatatable = js.native
  def DataTable(): JqueryDatatable = js.native
  def ajax: JqueryDatatable = js.native
  def reload(): JqueryDatatable = js.native
  def draw(): JqueryDatatable = js.native
  def row(selector: String): JqueryDatatable = js.native
  def row(jq: JQuery): JqueryDatatable = js.native
  def rows(): JqueryDatatable = js.native
  def rows(selector: String): JqueryDatatable = js.native
  def cell(): JqueryDatatable = js.native
  def cell(selector: String): JqueryDatatable = js.native
  def cells(): JqueryDatatable = js.native
  def cells(selector: String): JqueryDatatable = js.native
  def DataTable(init: Dynamic): JqueryDatatable = js.native
}

