import org.scalajs.jquery.JQuery

import scala.scalajs.js
import scala.scalajs.js.Dynamic

/**
  * @author Created by Sergey Dashko (sdashko@gmail.com) on 10/21/17.
  */
@js.native
trait JqueryDatepicker extends JQuery {
  def datetimepicker(): JqueryDatepicker = js.native
  def datetimepicker(option: String): JqueryDatepicker = js.native
  def datetimepicker(function: String, data: JqueryDatepicker): JqueryDatepicker = js.native
  def date: JqueryDatepicker = js.native
  def format(frmt: String): JqueryDatepicker = js.native
  def oldDate: JqueryDatepicker = js.native
  def datetimepicker(init: Dynamic): JqueryDatepicker = js.native
}

