import org.scalajs.dom
import org.scalajs.dom.ext.{Ajax, AjaxException}

import scala.scalajs.js.Dynamic.{literal => JC}
import scala.concurrent.ExecutionContext.Implicits.{global => ec}
import scala.scalajs.js
import org.scalajs.jquery.{JQueryEventObject, jQuery => $}

import scala.concurrent.Future
import scala.scalajs.js.Dynamic.global
import scala.scalajs.js.{Any, Dynamic, JSON}

trait TreeTable { self: GeneralFunctions =>
  import GeneralStructures._

  def treeTableEditHandler(selector: String, editNodeUrl: (Long) => String, editFolderUrl: (Long) => String) = {
    $(selector).on("click", ".edit", (event: JQueryEventObject) => {
      val trs = $(event.target).parents("tr")
      if (trs.length > 0) {
        val tr = trs(0)
        val id = $(tr).data("id").asInstanceOf[String].trim.toLong
        val folder: Option[Boolean] = $(tr).data("folder").asInstanceOf[js.UndefOr[Boolean]].toOption
        if (folder.nonEmpty && folder.get == true) {
          global.window.location = editFolderUrl(id)
        } else {
          global.window.location = editNodeUrl(id)
        }
      }

    })
  }

  def treeTableDeleteNodeInit(selector: String, url: (Long) => String) = {
    $(selector).on("click", ".delete", (event: JQueryEventObject) => {
      val trs = $(event.target).parents("tr")
      if (trs.length > 0) {
        val tr = trs(0)
        val id = $(tr).data("id").asInstanceOf[String]
        //        val folder: Option[Boolean] = $(tr).data("folder").asInstanceOf[js.UndefOr[Boolean]].toOption
        val node = global.findFancyNode(selector, id)
        Ajax.get(url(id.toLong)).map { xhr =>
          val answer = JSON.parse(xhr.responseText)
          if (answer.result.toString == "success") {
            node.remove()
          }
        }.recover{
          case e: AjaxException if e.xhr.status == 400 =>
            global.swal(
              JC(
                title = $("#nodeHasChildren").text()
              ))
          case e: AjaxException if e.xhr.status == 404 =>

//            global.window.location.reload()
          case e =>
            global.swal(
              JC(
                title = $("#someErrorHappens").text()
              ))
        }
      }

    })
  }


  def treeTableAddNode(eventObject: JQueryEventObject, treeId: String): Future[Boolean] = {
    eventObject.preventDefault()
    val tree = $(treeId).fancytree("getTree")
    val selectedNodes = tree.getSelectedNodes().asInstanceOf[js.Array[JqueryFancytree]]
    val maybeSelected = selectedNodes.headOption
    val parentNode = maybeSelected.getOrElse($(treeId).fancytree("getRootNode"))
    val form = $(eventObject.target).closest("form")
    val formData = form.serializeArray()
    val saveUrl = form.prop("action").toString
    val csrf = $("input[name=csrfToken]").`val`()
    val dataToPost = maybeSelected.map { node =>
      formData.push(
        JC(
          name = "parentId",
          value = node.key.toLong
        )
      )
      formData
    }.getOrElse {
      formData
    }
    val dataToPostTuples: Seq[(String, Any)] = dataToPost.map { data =>
      val d = data.asInstanceOf[js.Dynamic]
      val objectKey = d.name.asInstanceOf[String]
      val value = d.value.asInstanceOf[js.Any]
      (objectKey, value)
    }
    val dataToPostObject = js.Dictionary.apply(dataToPostTuples: _ *)
    val l = global.Ladda.create(eventObject.currentTarget)
    l.start()

    Ajax.post(saveUrl, data = JSON.stringify(dataToPostObject), headers = Map("Content-Type" -> "application/json", "Csrf-Token" -> csrf.toString)).map { xhr =>
      val answer = JSON.parse(xhr.responseText)
      if (answer.result.toString == "success") {
        parentNode.addChildren(answer.node)
        parentNode.renderStatus()
        form.find(".btnClear").trigger("click")
        true
      } else {
        form.parent("div").html(answer.form.toString)
        false
      }
    }.transform (
      s => {
        l.stop()
        s
      },
      ex => {
        l.stop()
        global.console.log(ex.getMessage)
        js.Dynamic.global.swal(
          JC(
            title = $("#someErrorHappens").text
          ))
        ex
      }
    )

  }

  def initFilterTree(treeId: String, inputId: String, clearButtonId: String) = {
    $(inputId).keyup((evnt: JQueryEventObject) => {
      val search = $(evnt.target).`val`()
      $(treeId).fancytree("getTree").filterNodes(search.toString, JC(
        autoExpand = true,
        mode = "hide"
      ))
      $(clearButtonId).prop("disabled", false)
      $(clearButtonId).prop("disabled", false)
    })

    $(clearButtonId).click((evnt: JQueryEventObject) => {
      $(inputId).`val`("")
      $(treeId).fancytree("getTree").clearFilter()
      $(clearButtonId).prop("disabled", true)
    })
  }


    def initTableTree(id: String, treeReloadUrl: String, renderColumns: (JQueryEventObject, Dynamic) => Unit): JqueryFancytree = {
      $(id).fancytree(
        JC(
          checkbox = "radio",
          selectMode = 1,
          extensions = js.Array("table", "glyph", "filter"),
          glyph = glyphOpts,
          //        wide = JC(
          //          iconWidth = "1em", // Adjust this if @fancy-icon-width != "16px"
          //          iconSpacing = "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
          //          labelSpacing = "0.1em", // Adjust this if padding between icon and label != "3px"
          //          levelOfs = "1.5em" // Adjust this if ul padding != "16px"
          //        ),
          source = JC(
            url = treeReloadUrl,
            cache = false
          ),
          //        select = (_: JQueryEventObject, data: Dynamic) => {
          //          val selectedId: js.UndefOr[Int] = data.node.data.id.asInstanceOf[js.UndefOr[Int]]
          //          val selected: js.UndefOr[Boolean] = data.node.selected.asInstanceOf[js.UndefOr[Boolean]]
          //          global.console.log(data.node)
          //          for {
          //            id <- selectedId.toOption
          //            isSelected <- selected.toOption
          //          } {
          //            //            global.console.log(id)
          //            if (isSelected) {
          //              $("#selectedNode").value(id.toString)
          //            } else {
          //              $("#selectedNode").value("")
          //            }
          //
          //          }
          //        },
          table = JC(
            checkboxColumnIdx = 0, // render the checkboxes into the this column index (default: nodeColumnIdx)
            indentation = 16, // indent every node level by 16px
            nodeColumnIdx = 1 // render node expander, icon, and title to this column (default: #0)
          ),
          //        fixed = JC(
          //          fixCols = 0,    // Fix leftmost n columns
          //          fixRows = true  // Fix topmost n rows (true: whole <thead>)
          //    ),
          renderColumns = renderColumns,
          beforeActivate = (ev: JQueryEventObject, data: Dynamic) => false
        ))
    }


  def initTree(id: String, treeReloadUrl: String, saveUrl: String) = {
    $(id).fancytree(
      JC(
        extensions = js.Array("edit", "glyph", "wide", "filter"),
        glyph = glyphOpts,
        wide = JC(
          iconWidth = "1em", // Adjust this if @fancy-icon-width != "16px"
          iconSpacing = "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
          labelSpacing = "0.1em", // Adjust this if padding between icon and label != "3px"
          levelOfs = "1.5em" // Adjust this if ul padding != "16px"
        ),
        source = JC(
          url = treeReloadUrl,
          cache = false
        ),
        edit = JC(
          triggerStart = js.Array("f2", "dblclick", "shift+click", "mac+enter"),
          save = (event: JQueryEventObject, data: Dynamic) => {
            // Save data.input.val() or return false to keep editor open
            val id = data.node.data.id
            val csrf = $("input[name=csrfToken]").`val`()
            if (id != js.undefined) {
              //updating existing node
              global.console.log("updating existing node")
              val dt = JC(
                title = $(data.input).`val`(),
                id = id.toString.toLong
              )
              Ajax.post(saveUrl, data = JSON.stringify(dt), headers = Map("Content-Type" -> "application/json", "Csrf-Token" -> csrf.toString)).map { xhr =>
                val answer = JSON.parse(xhr.responseText)
                if (answer.result.toString != "success") global.window.location.reload()
              }.recover {
                // Recover from a failed error code into a successful future
                case dom.ext.AjaxException(_) => global.window.location.reload()
              }
            } else {
              //adding new node
              global.console.log("adding new node")
              val parent = data.node.getParent()
              //              global.console.log(parent)
              //              global.console.log(parent.key)
              if (parent.key != js.undefined) {
                val dt = JC(
                  title = $(data.input).`val`(),
                  parentId = parent.key.toString.toLong
                )
                Ajax.post(saveUrl, data = JSON.stringify(dt), headers = Map("Content-Type" -> "application/json", "Csrf-Token" -> csrf.toString)).map { xhr =>
                  val answer = JSON.parse(xhr.responseText)
                  if (answer.result.toString != "success") {
                    global.window.location.reload()
                  }
                  data.node.key = answer.id
                  data.node.data = JC(id = answer.id)
                  data.node.parent.folder = true
                  data.node.getParent().renderStatus()
                }.recover {
                  // Recover from a failed error code into a successful future
                  case dom.ext.AjaxException(_) =>
                    global.window.location.reload()
                }
              }

            }


            $(data.node.span).removeClass("pending")

            true
          },
          close = (event: JQueryEventObject, data: Dynamic) => {
            // Editor was removed
            if (data.save.toString == "true") {
              // Since we started an async request, mark the node as preliminary
              $(data.node.span).addClass("pending")
            }
          }
        )
      )
    )
  }

}
