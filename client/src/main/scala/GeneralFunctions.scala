import org.scalajs.dom.ext.Ajax

import scala.scalajs.js.Dynamic.{global, literal => JC}
import org.scalajs.jquery.{jQuery => $}

import scala.concurrent.ExecutionContext.Implicits.{global => ec}
import scala.concurrent.Future
import scala.scalajs.js

trait GeneralFunctions{
  import GeneralStructures._


  def initSelect2(id: String): JqueryDatatable = {
    $(s"#$id").select2(
      JC(
        theme = "bootstrap"
      )
    )
  }

  def initSelect2(id: String, customOptions: js.Dynamic): JqueryDatatable = {
    $(s"#$id").select2(
      customOptions
    )
  }

  def initDatePicker(id: String): JqueryDatatable = {
    $(s"#$id").datetimepicker(
      JC(
        locale = "ru"
      )
    )
  }

  def initDatePicker(id: String, customOptions: js.Dynamic): JqueryDatatable = {
    $(s"#$id").datetimepicker(
      customOptions
    )
  }

  def datatable() = {
    val check = $("#datatable")
    if (check.length > 0) {
      datatableLangUrl(init)
    }

    def init = (langUrl: String) => {
      $("#datatable").DataTable(
        JC(language = JC(url = langUrl))
      )
    }
  }

  def datatableLangUrl(initDT: (String) => JqueryDatatable): Future[JqueryDatatable] = {
    val datatableLangUrl = global.jsRoutes.controllers.JsHelperController.datatableLangFileUrl()
    Ajax.get(datatableLangUrl.absoluteURL().toString).map { xhr =>
      initDT(xhr.responseText)
    }
  }





}
