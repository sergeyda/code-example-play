import org.scalajs.jquery.JQuery

import scala.scalajs.js
import scala.scalajs.js.Dynamic

/**
  * Created by sergey on 6/25/17.
  */
@js.native
trait JquerySelect2 extends JQuery {
  def select2(): JquerySelect2 = js.native
  def select2(param: String): JquerySelect2 = js.native
  def select2(init: Dynamic): JquerySelect2 = js.native
}

