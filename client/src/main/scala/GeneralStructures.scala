import org.scalajs.jquery.JQuery
import scala.scalajs.js.Dynamic.{literal => JC}
import scala.language.implicitConversions

object GeneralStructures {
  implicit def jq2datatable(jq: JQuery): JqueryDatatable = jq.asInstanceOf[JqueryDatatable]

  implicit def jq2fancytree(jq: JQuery): JqueryFancytree = jq.asInstanceOf[JqueryFancytree]

  implicit def jq2select2(jq: JQuery): JquerySelect2 = jq.asInstanceOf[JquerySelect2]
  implicit def jq2datepicker(jq: JQuery): JqueryDatepicker = jq.asInstanceOf[JqueryDatepicker]

  val glyphOpts = JC(
    preset = "bootstrap3",
    map = JC(
      doc = "glyphicon glyphicon-file",
      docOpen = "glyphicon glyphicon-file",
      checkbox = "glyphicon glyphicon-unchecked",
      checkboxSelected = "glyphicon glyphicon-check",
      checkboxUnknown = "glyphicon glyphicon-share",
      dragHelper = "glyphicon glyphicon-play",
      dropMarker = "glyphicon glyphicon-arrow-right",
      error = "glyphicon glyphicon-warning-sign",
      expanderClosed = "glyphicon glyphicon-menu-right",
      expanderLazy = "glyphicon glyphicon-menu-right", // glyphicon-plus-sign
      expanderOpen = "glyphicon glyphicon-menu-down", // glyphicon-collapse-down
      folder = "glyphicon glyphicon-folder-close",
      folderOpen = "glyphicon glyphicon-folder-open",
      loading = "glyphicon glyphicon-refresh glyphicon-spin"
    )
  )
}
