import org.scalajs.dom.ext.Ajax
import org.scalajs.jquery.{JQueryEventObject, jQuery => $}

import scala.concurrent.ExecutionContext.Implicits.{global => ec}
import scala.language.implicitConversions
import scala.scalajs.js
import scala.scalajs.js.Dynamic
import scala.scalajs.js.Dynamic.{global, literal => JC}
class GoodsImp extends Goods with GeneralFunctions with TreeTable

trait Goods extends Page{ self: GeneralFunctions with TreeTable =>
  import GeneralStructures._

  def init(): Unit = {
    goodsTree()
    goodsForms()
  }

  private def goodsTree() = {
    val check = $("#goods-tree")
    if (check.length > 0) {
      val url = global.jsRoutes.controllers.GoodsController.tree().absoluteURL().toString
      initTableTree("#goods-tree", url,(_: JQueryEventObject, data: Dynamic) => {
        val node: Dynamic = data.node
        $(node.tr).data("id", node.key)
        $(node.tr).data("folder", node.folder)
        val tdList = $(node.tr).find(">td")
        val code = node.data.code.asInstanceOf[js.UndefOr[String]].toOption
        val barcode = node.data.barcode.asInstanceOf[js.UndefOr[String]].toOption
        val unit = node.data.unit.asInstanceOf[js.UndefOr[String]].toOption
        code.foreach(tdList.eq(2).text(_))
        barcode.foreach(tdList.eq(3).text(_))
        unit.foreach(tdList.eq(4).text(_))
      })
      initFilterTree("#goods-tree", "#searchInput", "#clearFilter")
      val editUrl = (id: Long) => global.jsRoutes.controllers.GoodsController.edit(id).absoluteURL().toString
      val editFolderUrl = (id: Long) => global.jsRoutes.controllers.GoodsController.renderEditFolder(id).absoluteURL().toString
      treeTableEditHandler("#goods-tree", editUrl, editFolderUrl)
      val deleteUrl = (id: Long) => global.jsRoutes.controllers.GoodsController.delete(id).absoluteURL().toString
      treeTableDeleteNodeInit("#goods-tree", deleteUrl)
    }
  }

  private def goodsForms() = {
    val check = $("#goods-tree")
    if (check.length > 0) {
    reloadAddGoodForm()
    reloadAddGoodFolderForm()
    $("#addFolderFormPlacement").on("click", ".btnSubmit", (event: JQueryEventObject) => {
      treeTableAddNode(event, "#goods-tree")
    })
    $("#addFolderFormPlacement").on("click", ".btnClear", (event: JQueryEventObject) => {
      event.preventDefault()
      reloadAddGoodFolderForm()
    })
    $("#addGoodFormPlacement").on("click", ".btnClear", (event: JQueryEventObject) => {
      event.preventDefault()
      reloadAddGoodForm()
    })
    $("#addGoodFormPlacement").on("click", ".btnSubmit", (event: JQueryEventObject) => {
      event.preventDefault()
      val tree = $("#goods-tree").fancytree("getTree")
      val selectedNodes = tree.getSelectedNodes().asInstanceOf[js.Array[JqueryFancytree]]
      val maybeSelected = selectedNodes.headOption
      maybeSelected.map { _ =>
        treeTableAddNode(event, "#goods-tree")
      }.getOrElse {
        val alertText = $("#noParentSelected").text()
        js.Dynamic.global.swal(
          JC(
            title = alertText
          ))
      }

    })
    }
  }

  private def reloadAddGoodForm() = {
    val goodUrl = global.jsRoutes.controllers.GoodsController.renderAddForm().absoluteURL().toString
    Ajax.get(goodUrl).map { xhr =>
      $("#addGoodFormPlacement").html(xhr.responseText)
    }
  }

  private def reloadAddGoodFolderForm() = {
    val folderUrl = global.jsRoutes.controllers.GoodsController.renderAddFolder().absoluteURL().toString
    Ajax.get(folderUrl).map { xhr =>
      $("#addFolderFormPlacement").html(xhr.responseText)
    }
  }

}
