
import scala.language.implicitConversions
import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}


/**
  * Created by sergey on 6/17/17.
  */
@JSExportTopLevel("Front")
object Front extends GeneralFunctions
 {


  @JSExport
  def main(args: Array[String]): Unit = {
    datatable()
    new GoodsImp().init()
  }
}
