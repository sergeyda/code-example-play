import org.scalajs.dom.raw.EventTarget

import scala.scalajs.js
import scala.scalajs.js.Dynamic

@js.native
trait DatatableEditor extends js.Object{
  def inline(target: Dynamic, conf: Dynamic): DatatableEditor = js.native
}

