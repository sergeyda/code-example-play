/**
  * Created by sergey on 6/19/17.
  */
package object users {
    type Login = String
    type Password = String
    type Session = String
    type PasswordHash = String
    type Name = String
}
