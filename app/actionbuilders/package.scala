import cats.data.OptionT
import controllers.routes
import models.UserFront
import monix.execution.Scheduler
import monix.cats._
import monix.eval.Task
import play.api.mvc._
import play.api.mvc.Results.Redirect
import repository.UserRepository
import scala.concurrent.{ExecutionContext, Future}
import play.api.routing.Router

/**
  * Created by sergey on 6/23/17.
  */
package object actionbuilders {
  def OnlyAuth(rep: UserRepository)(scheduler: Scheduler)(
      implicit ec: ExecutionContext) = new ActionFilter[Request] {
    def executionContext = ec

    def filter[A](input: Request[A]) = {
      val res: Option[Task[Option[Result]]] = for {
        userId <- input.session.get("user")
        token <- input.session.get("token")
      } yield {
        {
          for {
            u <- OptionT(rep.findOne(userId.toLong))
            session <- OptionT(Task.now(u.session))
            if session != token
          } yield Redirect(routes.AuthController.login()).withNewSession
        }.value
      }

      res.map { _.runAsync(scheduler)}
        .getOrElse(Future.successful(
          Some(Redirect(routes.AuthController.login()).withNewSession)))
    }
  }

  def OnlyAuth(implicit ec: ExecutionContext) = new ActionFilter[UserRequest] {
    def executionContext = ec

    def filter[A](input: UserRequest[A]) = Future.successful {
        input.user.fold[Option[Result]](Some(Redirect(routes.AuthController.login()).withNewSession))((_: UserFront) => None)
    }
  }

  def OnlyAllowed(implicit ec: ExecutionContext) =
    new ActionFilter[UserRequest] {
      def check(modifiers: Seq[String], userRole: String): Boolean = {
        val allowedRoles: Seq[String] =
          for (modifier <- modifiers; role <- modifier.split(",")) yield role
        allowedRoles.contains(userRole)
      }

      def executionContext = ec

      def filter[A](input: UserRequest[A]) = Future.successful {
        input.user
          .map { u =>
            val handler = input.attrs(Router.Attrs.HandlerDef)
            val modifiers = handler.modifiers
            if (check(modifiers, u.roleName)) None
            else Some(Redirect(routes.HomeController.home()))
          }
          .getOrElse(Some(
            Redirect(routes.AuthController.login()).withNewSession))
      }
    }


  def OnlyNotAuth(implicit ec: ExecutionContext) = new ActionFilter[Request] {
    def executionContext = ec

    def filter[A](input: Request[A]) =
      Future.successful {
        for {
          _ <- input.session.get("user")
          _ <- input.session.get("token")
        } yield Redirect(routes.HomeController.home())
      }
  }
}
