package actionbuilders

/**
  * Created by sergey on 6/23/17.
  */
import cats.data.OptionT
import models.UserFront
import monix.execution.Scheduler
import monix.cats._
import monix.eval.Task
import play.api.mvc._
import repository.UserRepository

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

class UserRequest[A](val user: Option[UserFront], val request: Request[A])
    extends WrappedRequest[A](request)

case class UserAction[A](parser: BodyParser[A])(
    scheduler: Scheduler,
    rep: UserRepository)(implicit ec: ExecutionContext)
    extends ActionBuilder[UserRequest, A]
    with ActionTransformer[Request, UserRequest] {
  def executionContext = ec
  def transform[B](request: Request[B]) = {
    val res: Option[Task[Option[UserFront]]] = for {
      userId <- request.session.get("user")
      token <- request.session.get("token")
    } yield {
      {
        for {
          u <- OptionT(rep.findOneFront(userId.toLong))
          session <- OptionT(Task.now(u.session))
          if session == token
        } yield u
      }.value
    }

    res
      .map { task =>
        task.materialize
          .map {
            case Success(Some(u: UserFront)) =>
              new UserRequest(Some(u), request)
            case _ => new UserRequest(None, request)
          }
          .runAsync(scheduler)
      }
      .getOrElse(Future.successful(new UserRequest(None, request)))
  }
}
