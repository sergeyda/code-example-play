package actionbuilders

/**
  * @author Created by Sergey Dashko (sdashko@gmail.com) on 10/22/17.
  */
import models.Employee
import monix.execution.Scheduler
import monix.cats._
import play.api.mvc._
import repository.EmployeeRepository

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

class EmployeeRequest[A](val employee: Option[Employee], userRequest: UserRequest[A]) extends UserRequest[A](userRequest.user, userRequest)


case class EmployeeAction [A](scheduler: Scheduler, rep: EmployeeRepository)(implicit ec: ExecutionContext)
  extends ActionTransformer[UserRequest, EmployeeRequest] {
  def executionContext = ec
  def transform[B](request: UserRequest[B]) = {
    val res = request.user.map{ user =>
      rep.findOneByUserId(user.id)
    }

    res.map{ task =>
      task.materialize.map{
        case Success(Some(employee: Employee)) => new EmployeeRequest(Some(employee), request)
        case _ => new EmployeeRequest(None, request)
      }.runAsync(scheduler)
    }.getOrElse(Future.successful(new EmployeeRequest(None, request)))
  }
}