package viewcomponents

import controllers.routes
import models.Role

import scala.collection.mutable.LinkedHashMap

/**
  * Created by sergey on 6/24/17.
  */

case class Menu(title: String, roles: Set[String])
object MenuComponent {
  private val menuMap = LinkedHashMap[String, Menu](
    routes.HomeController.home().url -> Menu("Home", Set("Admin", "Cashier")),
    routes.UsersController.list().url -> Menu("Users", Set("Admin")),
    routes.EmployeesController.list().url -> Menu("Employees", Set("Admin")),
    routes.GoodsController.list().url -> Menu("Goods", Set("Admin", "Cashier")),
    routes.UnitsController.list().url -> Menu("Units", Set("Admin")),
  )

  def get(role: Role): collection.Map[String, String] = {
    menuMap.filter{case (_, menu) => menu.roles.contains(role.name)}.mapValues(_.title)
  }
}
