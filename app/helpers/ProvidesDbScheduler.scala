package helpers

import contexts.DbExecutionContext
import monix.execution.{Scheduler, UncaughtExceptionReporter}
import services.Mailer


/**
  * Created by sergey on 6/23/17.
  */
trait ProvidesDbScheduler{
  def dbEC: DbExecutionContext
  def mailer: Mailer
  lazy val uncaughtExceptionReporter =
    UncaughtExceptionReporter(mailer.sendErrorToAdmin)
  val scheduler: Scheduler = Scheduler(dbEC, uncaughtExceptionReporter)
}
