import models._
import play.api.data.Form
import play.api.libs.json._

import scala.util.Try


/**
  * Created by sergey on 6/30/17.
  */
package object helpers {

  def classToMap(cc: Product): Map[String, Any] = cc.getClass.getDeclaredFields.map( _.getName ) // all field names
    .zip( cc.productIterator.to ).toMap

  object LogHelper {

    case class LogJson (arguments: List[String])
    implicit class LoggableModelLog[T <: LoggableModel](val self: T) extends AnyVal {
      def logName: String = {
        self match {
          case _: User => "User"
          case _: Employee => "Employee"
          case _: Good => "Good.to.add"
          case _: Unit => "Unit"
          case other => throw new IllegalArgumentException(s"Can't find logName for ${other.getClass}")
        }
      }

      def logIdentity: String = {
        self match {
          case u: User => u.name
          case e: Employee => e.fullName
          case g: Good => g.title
          case u: Unit => u.title
          case other => throw new IllegalArgumentException(s"Can't find logIdentity for ${other.getClass}")
        }
      }
    }


    sealed trait LogOperation {
      override def toString: String = this match {
        case Added(_) => "Created"
        case Deleted(_) => "Deleted"
        case Updated(_, _) => "Updated"
      }

      def args: List[String] = this match {
        case Added(_) => List()
        case Deleted(obj: LoggableModel) => List(obj.logIdentity)
        case Updated(old: LoggableModel, newObj: LoggableModel) =>
          val oldMap: Map[String, Any] = classToMap(old.asInstanceOf[Product])
          val newMap: Map[String, Any] = classToMap(newObj.asInstanceOf[Product])
          val stringify  = (tpl: (String, Any)) => tpl match {
            case (title, value) => title + " = " + value.toString
          }
          List(old.logIdentity,
            oldMap.toList.diff(newMap.toList).map(stringify).mkString(", "),
            newMap.toList.diff(oldMap.toList).map(stringify).mkString(", ")
          )
      }
    }

    case class Added(obj: LoggableModel) extends LogOperation

    case class Deleted(obj: LoggableModel) extends LogOperation

    case class Updated(oldObj: LoggableModel, newObj: LoggableModel) extends LogOperation

    implicit val logJsonReads = Json.reads[LogJson]
    implicit val logJsonWrites = Json.writes[LogJson]

  }

  object DataTableEditor {
    def extractDataFromBody(data: Map[String, Seq[String]]): Iterable[Option[((Long, String), String)]] = {
        val filtered = data.filter{case (key, _) => key.startsWith("data") && !key.endsWith("action]")}
      filtered.map{case (key, value) =>
        val reg = "data\\[(\\d+)\\]\\[(\\S+)\\]".r
          val reg(id, field) = key
        val longId = Try(id.toLong).toOption
        longId.map( i => ((i, field), value.head))
      }
    }

    def formErrorsToJson(form: Form[_])(implicit provider: play.api.i18n.MessagesProvider) = {
      val messages = provider.messages
      Json.toJson("fieldErrors" -> JsArray(
        form.errors.map{ formErr =>
          Json.obj(
            "name" -> JsString(formErr.key),
            "status" -> JsString(messages(formErr.messages.headOption.getOrElse("")))
          )
        }
      ))
    }
  }

}
