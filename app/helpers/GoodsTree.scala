package helpers

import cats.free.Trampoline
import cats.implicits._
import models._

import scala.annotation.tailrec

/**
  * Created by sergey on 7/4/17.
  */
trait GoodsTree {
  val title: String
  val id: Long
  val folder: Boolean
}
trait GoodNode extends GoodsTree{
  val inner: List[GoodsTree]
}
trait GoodLeaf extends GoodsTree{
  val code: Option[String]
  val barcode: Option[String]
  val unit: String
}

case class GoodNodeImp(title: String, inner: List[GoodsTree], id: Long, folder: Boolean) extends GoodNode
case class GoodLeafImp(title: String, id: Long, folder: Boolean, code: Option[String], barcode: Option[String], unit: String) extends GoodLeaf

object GoodsTree{
  def apply(ls: List[GoodWithUnit]): List[GoodsTree] = {
    def build(ls: List[GoodWithUnit], current: GoodWithUnit): Trampoline[GoodsTree] = {
      if(!current.folder){
        Trampoline.done(GoodLeafImp(title = current.title, id = current.id, folder = current.folder, code = current.code, barcode = current.barcode, unit = current.unit.get))
      } else {
        val (children, newLs) = ls.partition{_.parentId.contains(current.id)}
        children.traverse(build(newLs, _)).map(trees => GoodNodeImp(title = current.title, id = current.id, inner = trees, folder = current.folder))
    }
  }
    val (roots, others) = ls.partition{ v => v.parentId.isEmpty}
    roots.map(build(others, _).run)
  }

  import play.api.libs.json._

  implicit val goodTreeWrites: Writes[GoodsTree] = {
    case node: GoodNode =>
      Json.obj(
        "title" -> JsString(node.title),
        "id" -> JsString(node.id.toString),
        "key" -> JsString(node.id.toString),
        "checkbox" -> JsBoolean(true),
        "folder" -> JsBoolean(true),
        "children" -> JsArray(node.inner.map(child => Json.toJson(child)))
      )
    case l: GoodLeaf =>
        Json.obj(
          "title" -> JsString(l.title),
          "id" -> JsString(l.id.toString),
          "key" -> JsString(l.id.toString),
          "unit" -> JsString(l.unit),
          "code" -> JsString(l.code.getOrElse("")),
          "barcode" -> JsString(l.barcode.getOrElse("")),
          "folder" -> JsBoolean(false),
          "checkbox" -> JsBoolean(false)
        )
  }

}


