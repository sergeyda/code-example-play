package helpers

import models.TreeStructureModel
import scala.annotation.tailrec

/**
  * @author Created by Sergey Dashko (sdashko@gmail.com) on 10/31/17.
  */


object FlattenTree {

  case class Leaf[+T <: TreeStructureModel](path: String, title: String, id: Long)

  def apply[T <: TreeStructureModel](ls: List[T]): List[Leaf[T]] = {
    @tailrec
    def build(toProcess: List[T], accum: List[Leaf[T]], currentParentId: Option[Long]): List[Leaf[T]] = {
      currentParentId match {
        case None => toProcess match {
          case head :: tail => build(tail, Leaf("", head.title, head.id) :: accum, head.parentId)
          case Nil => accum
        }
        case Some(parentId) =>
          val parent = ls.find(_.id == parentId).getOrElse(throw new RuntimeException(s"Can't find node with id $parentId"))
          build(toProcess, accum.head.copy(path = parent.title + "/" + accum.head.path) :: accum.tail, parent.parentId)
      }
    }

    val onlyChildren = ls.filterNot(_.folder)
    build(onlyChildren, List[Leaf[T]](), None)
  }

  import play.api.libs.json._


  implicit def flattenTreeWrites[T <: TreeStructureModel]: Writes[Leaf[T]] = {
    Writes(
      (leaf: Leaf[T]) =>
        Json.toJson(
          Json.obj(
            "text" -> JsString(leaf.title),
            "path" -> JsString(leaf.path),
            "id" -> JsNumber(leaf.id)
          )
        )
    )
  }

}


