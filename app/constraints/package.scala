import java.time.LocalDate
import java.time.format.DateTimeFormatter

import play.api.data.validation._
import repository.UserRepository

import scala.util.Try

/**
  * Created by sergey on 6/26/17.
  */
package object constraints {
  val allNumbers = """\d*""".r
  val allLetters = """[A-Za-z]*""".r
  val phone = """\d{3,3}-\d{3,3}-\d{2,2}-\d{2,2}""".r

  val passwordCheckConstraint: Constraint[String] =
  Constraint("constraints.passwordcheck")({ plainText =>
    val errors = plainText match {
      case allNumbers() => Seq(ValidationError("password.all.numbers"))
      case allLetters() => Seq(ValidationError("password.all.letters"))
      case _ => Nil
    }
    if (errors.isEmpty) {
      Valid
    } else {
      Invalid(errors)
    }
  })

  val phoneCheckConstraint: Constraint[Option[String]] =
    Constraint("constraints.phonecheck")({ opt =>
      opt
        .map { plainText =>
          val errors = plainText match {
            case phone() => Nil
            case _ => Seq(ValidationError("phone.format.incorrect"))
          }
          if (errors.isEmpty) {
            Valid
          } else {
            Invalid(errors)
          }
        }
        .getOrElse(Valid)
    })

  def correctDateConstraint(formatter: DateTimeFormatter): Constraint[String] =
    Constraint("constraints.date")({ plainText =>
      val result = Try(LocalDate.parse(plainText, formatter)).toOption
      if (result.nonEmpty) {
        Valid
      } else {
        Invalid(Seq(ValidationError("date.format.incorrect")))
      }
    })

  def optionalDateConstraint(
                              formatter: DateTimeFormatter): Constraint[Option[String]] =
    Constraint("constraints.date")(opt =>
      opt
        .map { string =>
          val result = Try(LocalDate.parse(string, formatter)).toOption
          if (result.nonEmpty) {
            Valid
          } else {
            Invalid(Seq(ValidationError("error.date.format.incorrect")))
          }
        }
        .getOrElse(Valid)
    )

}
