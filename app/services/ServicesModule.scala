package services

import play.api.BuiltInComponents
import play.api.libs.mailer.MailerComponents
import repository.RepositoryModule

trait ServicesModule{ this: MailerComponents with BuiltInComponents with RepositoryModule =>

  import com.softwaremill.macwire._

  lazy val dbService = wire[DbImplem]
  lazy val mailerService = wire[MailerImpl]

}
