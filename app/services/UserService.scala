package services

import java.security.SecureRandom
import javax.crypto.spec.PBEKeySpec
import javax.crypto.{SecretKey, SecretKeyFactory}

import actionbuilders.UserRequest
import cats.data.OptionT
import org.apache.commons.codec.binary.Base64
import models.Employee
import monix.eval.Task
import repository.{EmployeeRepository, LogRepository, UserRepository}
import users._
import cats.implicits._
import monix.cats._

import scala.util.{Failure, Random, Success, Try}

/**
  * Created by sergey on 6/19/17.
  */
trait UserService {
  def passwordHash(password: Password): Try[String]
  def sessionToken: String
  def generatePassword(length: Int): String
  def checkPassword(password: Password, stored: PasswordHash): Try[Boolean]
  def unLinkEmployee(userId: Long)(
    implicit userRep: UserRepository,
    emplRep: EmployeeRepository,
    logRep: LogRepository,
    request: UserRequest[_]): Task[Option[Employee]]
  def linkEmployee(userId: Long, emplId: Long)(
    implicit userRep: UserRepository,
    emplRep: EmployeeRepository,
    logRep: LogRepository,
    request: UserRequest[_]
  ): Task[Option[Employee]]
}

class UserServiceImpl extends UserService {
  private val iterations: Int = 20 * 1000
  private val saltLen: Int = 32
  private val desiredKeyLen: Int = 256

  def passwordHash(password: Password): Try[String] = {
    val salt = SecureRandom.getInstance("SHA1PRNG").generateSeed(saltLen)
    // store the salt with the password
    hash(password, salt).map(Base64.encodeBase64String(salt) + "$" + _)
  }

  /** Checks whether given plaintext password corresponds
      to a stored salted hash of the password. */
  def checkPassword(password: Password, stored: PasswordHash): Try[Boolean] = {
    val saltAndPass: Array[String] = stored.split("\\$")
    if (saltAndPass.length != 2) {
      Failure(
        new IllegalStateException(
          "The stored password should have the form 'salt$hash'"))
    } else {
      val hashOfInput = hash(password, Base64.decodeBase64(saltAndPass(0)))
      hashOfInput.map(_.equals(saltAndPass(1))) match {
        case Success(r) => Success(r)
        case Failure(e) => Failure(e)
      }
    }
  }

  // using PBKDF2 from Sun, an alternative is https://github.com/wg/scrypt
  // cf. http://www.unlimitednovelty.com/2012/03/dont-use-bcrypt.html
  private def hash(password: String, salt: Array[Byte]): Try[String] = {
    if (password == null || password.length() == 0) {
      Failure(
        new IllegalArgumentException("Empty passwords are not supported."))
    } else {
      val f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
      val key: SecretKey = f.generateSecret(
        new PBEKeySpec(password.toCharArray, salt, iterations, desiredKeyLen))
      Success(Base64.encodeBase64String(key.getEncoded))
    }
  }

  def generatePassword(length: Int): String = {
    val x = Random.alphanumeric
    x.take(length).mkString
  }

  def sessionToken: String =
    Base64.encodeBase64String(
      SecureRandom.getInstance("SHA1PRNG").generateSeed(saltLen))

  def linkEmployee(userId: Long, emplId: Long)(
      implicit userRep: UserRepository,
      emplRep: EmployeeRepository,
      logRep: LogRepository,
      request: UserRequest[_]
  ): Task[Option[Employee]] = {

    val employee = for {
      _ <- OptionT(userRep.findOne(userId))
      empl <- OptionT(emplRep.findOne(emplId))
      updatedEmployee = empl.copy(userId = Some(userId))
      r <- OptionT.liftF(emplRep.updateAndLog(empl, updatedEmployee))
    } yield r
    employee.value
  }

  def unLinkEmployee(userId: Long)(
      implicit userRep: UserRepository,
      emplRep: EmployeeRepository,
      logRep: LogRepository,
      request: UserRequest[_]): Task[Option[Employee]] = {

    val employee: OptionT[Task, Employee] = for {
      _ <- OptionT(userRep.findOne(userId))
      empl <- OptionT(emplRep.findOneByUserId(userId))
      updatedEmployee = empl.copy(userId = None)
      r <- OptionT.liftF(emplRep.updateAndLog(empl, updatedEmployee))
    } yield r
    employee.value
  }
}

object UserService extends UserServiceImpl
