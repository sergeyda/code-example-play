package services

/**
  * Created by sergey on 6/19/17.
  */

import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import doobie.hikari.hikaritransactor.HikariTransactor
import doobie.imports._
import monix.eval._
import monix.cats._
import fs2.util.{Attempt, Catchable, Suspendable}
import play.api.inject.ApplicationLifecycle
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}


object TaskCatchable extends Catchable[Task] with Suspendable[Task] {
  def pure[A](a: A): Task[A] = Task.pure(a)

  def flatMap[A, B](a: Task[A])(f: A => Task[B]): Task[B] =
    a.flatMap(f)

  def fail[A](err: Throwable): Task[A] =
    Task.raiseError(err)

  def attempt[A](fa: Task[A]): Task[Attempt[A]] =
    fa.materialize.map {
      case Success(value) => Right(value)
      case Failure(e) => Left(e)
    }

  def suspend[A](fa: => Task[A]): Task[A] =
    Task.suspend(fa)
}


sealed trait Db {
  def transactor: HikariTransactor[Task]
}

class DbImplem(configuration: play.api.Configuration, applicationLifecycle: ApplicationLifecycle) extends Db {
  implicit val tsk = TaskCatchable
  val config = new HikariConfig()
  config.setJdbcUrl(configuration.get[String]("db.default.url"))
  config.setUsername(configuration.get[String]("db.default.username"))
  config.setDriverClassName(configuration.get[String]("db.default.driver"))
  config.setMaximumPoolSize(configuration.get[Int]("doobie.poolMaxSize"))
  configuration.getOptional[String]("db.default.password").foreach { pass =>
    config.setPassword(pass)
  }

  applicationLifecycle.addStopHook { () =>
    stop()
  }

  val ds: HikariDataSource = new HikariDataSource(config)


  val transactor: HikariTransactor[Task] = HikariTransactor[Task](ds)

  def stop(): Future[Unit] = {
    Future(if (!ds.isClosed) ds.close())
  }
}

