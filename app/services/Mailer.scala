package services

import play.api.libs.mailer._
import monix.eval.Task
import monix.execution.Scheduler

import scala.util.Failure
/**
  * Created by sergey on 6/22/17.
  */
trait Mailer {
  def sendMail(email: String, subject: String, body: String): String
  def sendErrorToAdmin(e: Throwable): Unit
  def sendIfErrorToAdmin[T](task: Task[T])(implicit scheduler: Scheduler): Unit
}

class MailerImpl(mailer: MailerClient, configuration: play.api.Configuration) extends Mailer{
  def sendMail(email: String, subject: String, body: String): String = {
    val from = configuration.get[String]("play.mailer.user")
    val mail = Email(
      subject = subject,
      from = from,
      bodyText = Some(body),
      to = Seq(email)
    )
    mailer.send(mail)
  }

  def sendErrorToAdmin(e: Throwable): Unit = {
    val adminMail = configuration.get[String]("admin.email")
    val appDomain = configuration.get[String]("application.domain")
    sendMail(
      body = s"There was error in application on domain $appDomain:" +
        s"\n ${e.getMessage}",
      subject = s"There was error in application on domain $appDomain",
      email = adminMail
    )
  }

  def sendIfErrorToAdmin[T](task: Task[T])(implicit scheduler: Scheduler): Unit = {
    task.materialize.map{
      case Failure(e) => sendErrorToAdmin(e)
      case _ =>
    }.runAsync
  }
}
