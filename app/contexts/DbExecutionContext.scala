package contexts

import akka.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext

import scala.concurrent.ExecutionContext
/**
  * Created by sergey on 6/23/17.
  */
trait DbExecutionContext extends ExecutionContext

class DbExecutionContextImpl (system: ActorSystem)
  extends CustomExecutionContext(system, "contexts.db") with DbExecutionContext
