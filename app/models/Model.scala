package models

import users._

/**
  * Created by sergey on 6/30/17.
  */
sealed trait LoggableModel

sealed trait TreeStructureModel {
  val parentId: Option[Long]
  val title: String
  val id: Long
  val folder: Boolean

  def updateTitle(title: String): TreeStructureModel
}

case class User(
                 id: Long = 0L,
                 login: Login,
                 name: Name,
                 passwordHash: PasswordHash,
                 session: Option[Session],
                 roleId: Long
               ) extends LoggableModel


case class Unit(id: Long = 0L, title: String) extends LoggableModel

case class Employee(
                     id: Long = 0L,
                     fullName: String,
                     address: Option[String],
                     email: Option[String],
                     phone: Option[String],
                     position: Option[String],
                     cardNumber: Option[String],
                     userId: Option[Long] = None
                   ) extends LoggableModel


case class Good(
                 id: Long = 0L,
                 title: String,
                 parentId: Option[Long],
                 unitId: Option[Long],
                 barcode: Option[String],
                 code: Option[String],
                 folder: Boolean
               ) extends LoggableModel with TreeStructureModel {
  def updateTitle(title: String): Good = this.copy(title = title)
}
