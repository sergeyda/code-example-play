package models

import java.sql.Timestamp
import java.util.Calendar

/**
  * Created by sergey on 6/19/17.
  */


case class Log (id: Long = 0L, `type`: String, text: String, hidden: Boolean, userId: Long, time: Timestamp = new java.sql.Timestamp(Calendar.getInstance().getTime.getTime))
