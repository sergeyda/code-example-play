package models

/**
  * Created by sergey on 6/19/17.
  */



import users._


case class UserFront(id: Long = 0L, login: Login, name: Name, session: Option[Session], roleId: Long, roleName: String)