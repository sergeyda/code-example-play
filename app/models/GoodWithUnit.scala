package models

case class GoodWithUnit (id: Long = 0L, title: String, parentId: Option[Long], unitId: Option[Long], barcode: Option[String], code: Option[String], folder: Boolean, unit: Option[String])
