package repository

import actionbuilders.UserRequest
import helpers.LogHelper.{Added, Deleted, LogJson, LogOperation, Updated}
import models.{Log, LoggableModel}
import monix.eval.Task
import monix.execution.Scheduler
import play.api.Logger
import play.api.libs.json.{JsValue, Json}
import helpers.LogHelper._
/**
  * @author Created by Sergey Dashko (sdashko@gmail.com) on 9/23/17.
  */

trait LoggableRepository[T <: LoggableModel] { this: Repository[T] =>

  def operationToLog(operation: LogOperation)(implicit userReq: UserRequest[_]): Log = {
    val user = userReq.user.getOrElse(throw new RuntimeException("Can't find user in request"))
    val (json: JsValue, logType: String) = operation match {
        case (del: Deleted) =>
          val logJson: JsValue = Json.toJson(LogJson(operation.args))
          val logType = s"log.${del.obj.logName}.$operation"
          (logJson, logType)

        case (added: Added)=>
          val logJson: JsValue = Json.toJson(LogJson(List(added.obj.logIdentity)))
          val logType = s"log.${added.obj.logName}.$operation"
          (logJson, logType)

        case (updated : Updated)=>
          val logJson: JsValue = Json.toJson(LogJson(operation.args))
          val logType = s"log.${updated.oldObj.logName}.$operation"
          (logJson, logType)

        case obj => throw new RuntimeException(s"Can't log object $obj")
      }

    Log(`type` = logType, text = json.toString(), hidden = false, userId = user.id)

  }


  def insertAndLog(obj: T)(implicit rep: LogRepository, userReq: UserRequest[_]): Task[T] = {
    for{
      model <- insert(obj)
      _ <- rep.insert(operationToLog(Added(obj)))
    } yield model
  }

  def updateAndLog(oldObj: T, newObj: T)(implicit rep: LogRepository, userReq: UserRequest[_]): Task[T] = {
    for{
      model <- update(newObj)
      _ <- rep.insert(operationToLog(Updated(oldObj, newObj)))
    } yield model
  }

  def deleteAndLog(obj: T)(implicit rep: LogRepository, userReq: UserRequest[_]): Task[Int] = {
    for{
      result <- delete(obj)
      _ <- rep.insert(operationToLog(Deleted(obj)))
    } yield result
  }
}
