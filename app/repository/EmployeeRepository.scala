package repository

import models.Employee
import monix.eval.Task

/**
  * Created by sergey on 6/19/17.
  */
trait EmployeeRepository extends Repository[Employee]  with LoggableRepository[Employee]{
  def withoutKassa: Task[List[Employee]]
  def withoutUser: Task[List[Employee]]
  def findOneByUserId(userId: Long): Task[Option[Employee]]
  def hasKassa(employeeId: Long): Task[Boolean]
}
