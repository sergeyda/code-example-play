package repository

import doobie.imports.ConnectionIO
import models.Log
import monix.eval.Task

/**
  * Created by sergey on 6/19/17.
  */
trait LogRepository extends Repository[Log] {
  def count(logType: Option[String], userId: Option[Long]): Task[Int]
  def listLogTypes(): Task[List[String]]
  def list(count: Int, offset: Int, logType: Option[String], userId: Option[Long]): Task[List[Log]]
  def createInsert(obj: Log): ConnectionIO[Log]
}
