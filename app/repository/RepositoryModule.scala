package repository

import play.api.BuiltInComponentsFromContext
import repository.implementations._
import services.ServicesModule


trait RepositoryModule{ this: BuiltInComponentsFromContext with ServicesModule =>

  import com.softwaremill.macwire._

  implicit lazy val userRepository: UserRepositoryImplem = wire[UserRepositoryImplem]
  implicit lazy val empRepository: EmployeeRepositoryImplem = wire[EmployeeRepositoryImplem]
  implicit lazy val goodRepository: GoodRepositoryImplem = wire[GoodRepositoryImplem]
  implicit lazy val unitRepository: UnitRepositoryImplem = wire[UnitRepositoryImplem]
  implicit lazy val roleRepository: RoleRepositoryImplem = wire[RoleRepositoryImplem]
  implicit lazy val logRepository: LogRepositoryImplem = wire[LogRepositoryImplem]
}
