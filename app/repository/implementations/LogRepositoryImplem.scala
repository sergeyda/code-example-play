package repository.implementations


import doobie.imports._
import models.Log
import monix.eval._
import repository.LogRepository
import services.Db
import cats._, cats.data._, cats.implicits._
import Fragments._

/**
  * Created by sergey on 6/24/17.
  */
class LogRepositoryImplem(val db: Db) extends LogRepository {
  def findOne(id: Long): Task[Option[Log]] = {
    sql"SELECT * FROM logs WHERE ID = $id AND hidden IS FALSE".asInstanceOf[Fragment]
      .query[Log]
      .option
      .transact(db.transactor)
  }


  def insert(obj: Log): Task[Log] = {
    sql"INSERT INTO logs (type, text, hidden, user_id, time) VALUES (${obj.`type`}, ${obj.text}, ${obj.hidden}, ${obj.userId}, ${obj.time})".asInstanceOf[Fragment]
      .update.withUniqueGeneratedKeys[Log]("id", "type", "text", "hidden", "user_id", "time")
      .transact(db.transactor)
  }

  def createInsert(obj: Log): ConnectionIO[Log] = {
    sql"INSERT INTO logs (type, text, hidden, user_id, time) VALUES (${obj.`type`}, ${obj.text}, ${obj.hidden}, ${obj.userId}, ${obj.time})".asInstanceOf[Fragment]
      .update.withUniqueGeneratedKeys[Log]("id", "type", "text", "hidden", "user_id", "time")
  }

  def update(obj: Log): Task[Log] = {
    sql"UPDATE logs SET (type, text, hidden, user_id, time) = (${obj.`type`}, ${obj.text}, ${obj.hidden}, ${obj.userId}, ${obj.time}) WHERE ID = ${obj.id}".asInstanceOf[Fragment]
      .update.withUniqueGeneratedKeys[Log]("id", "type", "text", "hidden", "user_id", "time")
      .transact(db.transactor)
  }

  def list(count: Int = 10000, offset: Int = 0): Task[List[Log]] = {
    sql"select * from logs WHERE hidden = 'false' ORDER BY time asc limit $count offset $offset".asInstanceOf[Fragment]
      .query[Log]
      .list
      .transact(db.transactor)
  }

  def listLogTypes(): Task[List[String]] = {
    sql"select type from logs WHERE hidden = 'false' GROUP BY type".asInstanceOf[Fragment]
      .query[String]
      .list
      .transact(db.transactor)
  }

  def list(count: Int, offset: Int, logType: Option[String], userId: Option[Long]): Task[List[Log]] = {
    val baseSql = fr"select * from logs WHERE hidden = 'false'".asInstanceOf[Fragment]
    val additionalSql = fr"ORDER BY time asc limit $count offset $offset".asInstanceOf[Fragment]
    val sql = andOpt(Some(baseSql), logTypeSql(logType), userIdSQL(userId)) ++ additionalSql
    sql
      .query[Log]
      .list
      .transact(db.transactor)
  }

  def count(logType: Option[String], userId: Option[Long]): Task[Int] = {
    val baseSql = fr"select COUNT(*) from logs WHERE hidden = 'false'".asInstanceOf[Fragment]
    val sql = andOpt(Some(baseSql), logTypeSql(logType), userIdSQL(userId))
    sql
      .query[Int]
      .unique
      .transact(db.transactor)
  }

  def delete(obj: Log): Task[Int] = {
    sql"DELETE FROM logs WHERE ID = ${obj.id}".asInstanceOf[Fragment]
      .update
      .run
      .transact(db.transactor)
  }

  private def logTypeSql(logType: Option[String]) = {
    logType.map { st =>
      fr"type = $st".asInstanceOf[Fragment]
    }
  }

  private def userIdSQL(userId: Option[Long]) = {
    userId.map{ id =>
      fr"user_id = $id".asInstanceOf[Fragment]
    }
  }
}
