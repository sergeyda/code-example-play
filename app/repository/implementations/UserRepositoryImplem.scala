package repository.implementations

import users._
import models.{User, UserFront}
import doobie.imports._
import repository.UserRepository
import services.Db
import monix.eval._
import monix.cats._

/**
  * Created by sergey on 6/19/17.
  */
class UserRepositoryImplem(val db: Db) extends UserRepository {
    def findOne(id: Long): Task[Option[User]] = {
      sql"select * from users WHERE ID = $id".asInstanceOf[Fragment]
        .query[User]
        .option
        .transact(db.transactor)
    }



  def findOneByLogin(login: Login): Task[Option[User]] = {
    sql"select * from users WHERE login = $login".asInstanceOf[Fragment]
      .query[User]
      .option
      .transact(db.transactor)
  }

    def insert(obj: User): Task[User] = {
      sql"insert into users (login, name, password_hash, session, role_id) values (${obj.login}, ${obj.name}, ${obj.passwordHash}, ${obj.session}, ${obj.roleId})".asInstanceOf[Fragment]
        .update.withUniqueGeneratedKeys[User]("id", "login", "name", "password_hash", "session", "role_id")
        .transact(db.transactor)
    }

    def update(obj: User): Task[User] = {
      sql"update users set (login, name, password_hash, session, role_id) = (${obj.login}, ${obj.name}, ${obj.passwordHash}, ${obj.session}, ${obj.roleId}) where ID = ${obj.id}".asInstanceOf[Fragment]
        .update.withUniqueGeneratedKeys[User]("id", "login", "name", "password_hash", "session", "role_id")
        .transact(db.transactor)
    }

  def list(count: Int = 10000, offset: Int = 0): Task[List[User]] = {
    sql"select * from users offset $offset limit $count".asInstanceOf[Fragment]
      .query[User]
      .list
      .transact(db.transactor)
  }

  def delete(obj: User): Task[Int] = {
    sql"DELETE FROM users where ID = ${obj.id}".asInstanceOf[Fragment]
      .update
      .run
      .transact(db.transactor)
  }

  def listFront(count: Int = 10000, offset: Int = 0): Task[List[UserFront]] = {
    sql"select users.id, users.login, users.name, users.session, roles.id, roles.name from users JOIN roles ON users.role_id = roles.id offset $offset limit $count".asInstanceOf[Fragment]
      .query[UserFront]
      .list
      .transact(db.transactor)
  }

  def findOneFront(id: Long): Task[Option[UserFront]] = {
    sql"select users.id, users.login, users.name, users.session, roles.id, roles.name from users JOIN roles ON users.role_id = roles.id  WHERE users.id = $id".asInstanceOf[Fragment]
      .query[UserFront]
      .option
      .transact(db.transactor)
  }
}
