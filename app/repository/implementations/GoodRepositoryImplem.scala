package repository.implementations

import doobie.imports._
import models.{Good, GoodWithUnit}
import monix.eval._
import repository.GoodRepository
import services.Db

/**
  * @author Created by Sergey Dashko (sdashko@gmail.com) on 7/26/17.
  */

class GoodRepositoryImplem(val db: Db) extends GoodRepository {
  def findOne(id: Long): Task[Option[Good]] = {
    sql"SELECT * FROM goods WHERE ID = $id".asInstanceOf[Fragment]
      .query[Good]
      .option
      .transact(db.transactor)
  }

  def findOneWithUnit(id: Long): Task[Option[GoodWithUnit]] = {
    sql"SELECT goods.*, units.title FROM goods LEFT JOIN units ON goods.unit_id = units.id WHERE goods.id = $id".asInstanceOf[Fragment]
      .query[GoodWithUnit]
      .option
      .transact(db.transactor)
  }


  def insert(obj: Good): Task[Good] = {
    sql"INSERT INTO goods (title, parent_id, unit_id, barcode, code, folder) VALUES (${obj.title}, ${obj.parentId}, ${obj.unitId}, ${obj.barcode}, ${obj.code}, ${obj.folder})".asInstanceOf[Fragment]
      .update.withUniqueGeneratedKeys[Good]("id", "title", "parent_id", "unit_id", "barcode", "code", "folder")
      .transact(db.transactor)
  }

  def update(obj: Good): Task[Good] = {
    sql"UPDATE goods SET (title, parent_id, unit_id, barcode, code, folder) = (${obj.title}, ${obj.parentId}, ${obj.unitId}, ${obj.barcode}, ${obj.code}, ${obj.folder}) WHERE ID = ${obj.id}".asInstanceOf[Fragment]
      .update.withUniqueGeneratedKeys[Good]("id", "title", "parent_id", "unit_id", "barcode", "code", "folder")
      .transact(db.transactor)
  }

  def list(cnt: Int, offs: Int = 0): Task[List[Good]] = {
    sql"SELECT * FROM goods OFFSET $offs LIMIT $cnt".asInstanceOf[Fragment]
      .query[Good]
      .list
      .transact(db.transactor)
  }

  def search(term: String): Task[List[Good]] = {
    sql"SELECT * FROM goods WHERE title ILIKE ${term + "%"}".asInstanceOf[Fragment]
      .query[Good]
      .list
      .transact(db.transactor)
  }

  def searchByKeywordWhere(term: String): Fragment =
    fr"WHERE title ILIKE ${term + "%"}".asInstanceOf[Fragment]

  def list(where: Fragment): Task[List[Good]] = {
    val base = fr"select * from goods".asInstanceOf[Fragment]
    val sql = base ++ where
    sql
      .query[Good]
      .list
      .transact(db.transactor)
  }

  def listWithParents(where: Fragment): Task[List[Good]] = {
    val base = fr"WITH RECURSIVE q AS(SELECT g.* from goods g".asInstanceOf[Fragment]
    val end = fr"	UNION SELECT gp.* from q JOIN goods gp ON gp.id = q.parent_id) Select * from q".asInstanceOf[Fragment]
    val sql = base ++ where ++ end
    sql
      .query[Good]
      .list
      .transact(db.transactor)
  }

  def listByParent(parentId: Long): Task[List[Good]] = {
    sql"SELECT * FROM goods WHERE parent_id = $parentId".asInstanceOf[Fragment]
      .query[Good]
      .list
      .transact(db.transactor)
  }

  def delete(obj: Good): Task[Int] = {
    sql"DELETE FROM goods WHERE ID = ${obj.id}".asInstanceOf[Fragment]
      .update
      .run
      .transact(db.transactor)
  }

  def listWithUnit(cnt: Int, offs: Int = 0): Task[List[GoodWithUnit]] = {
    sql"SELECT goods.*, units.title FROM goods LEFT JOIN units ON goods.unit_id = units.id OFFSET $offs LIMIT $cnt".asInstanceOf[Fragment]
      .query[GoodWithUnit]
      .list
      .transact(db.transactor)
  }
}
