package repository.implementations

import doobie.imports._
import models.Role
import monix.eval._
import repository.RoleRepository
import services.Db

/**
  * Created by sergey on 6/24/17.
  */
class RoleRepositoryImplem(val db: Db) extends RoleRepository {
    def findOne(id: Long): Task[Option[Role]] = {
      sql"select * from roles WHERE ID = $id".asInstanceOf[Fragment]
        .query[Role]
        .option
        .transact(db.transactor)
    }


    def insert(obj: Role): Task[Role] = {
      sql"insert into roles (name) values (${obj.name})".asInstanceOf[Fragment]
        .update.withUniqueGeneratedKeys[Role]("id", "name")
        .transact(db.transactor)
    }

    def update(obj: Role): Task[Role] = {
      sql"update roles set (name) = (${obj.name}) where ID = ${obj.id}".asInstanceOf[Fragment]
        .update.withUniqueGeneratedKeys[Role]("id", "name")
        .transact(db.transactor)
    }

  def list(count: Int = 10000, offset: Int = 0): Task[List[Role]] = {
    sql"select * from roles offset $offset limit $count".asInstanceOf[Fragment]
      .query[Role]
      .list
      .transact(db.transactor)
  }

  def delete(obj: Role): Task[Int] = {
    sql"DELETE FROM roles where ID = ${obj.id}".asInstanceOf[Fragment]
      .update
      .run
      .transact(db.transactor)
  }
}
