package repository.implementations

import doobie.imports._
import models.Employee
import monix.eval._
import repository.EmployeeRepository
import services.Db

/**
  * Created by sergey on 6/24/17.
  */
class EmployeeRepositoryImplem(val db: Db) extends EmployeeRepository {
  def findOne(id: Long): Task[Option[Employee]] = {
    sql"select * from employees WHERE ID = $id".asInstanceOf[Fragment]
      .query[Employee]
      .option
      .transact(db.transactor)
  }

  def findOneByUserId(userId: Long): Task[Option[Employee]] = {
    sql"select * from employees WHERE user_id = $userId".asInstanceOf[Fragment]
      .query[Employee]
      .option
      .transact(db.transactor)
  }

  def hasKassa(employeeId: Long): Task[Boolean] = {
    val emp = sql"SELECT * FROM employees JOIN kassa ON kassa.employee_id = employees.id WHERE employees.id = $employeeId".asInstanceOf[Fragment]
      .query[Employee]
      .option
      .transact(db.transactor)
    emp.map(_.isDefined)
  }


  def insert(obj: Employee): Task[Employee] = {
    sql"insert into employees (full_name, address, email, phone, position, card_number, user_id) values (${obj.fullName}, ${obj.address}, ${obj.email}, ${obj.phone}, ${obj.position}, ${obj.cardNumber}, ${obj.userId})".asInstanceOf[Fragment]
      .update.withUniqueGeneratedKeys[Employee]("id", "full_name", "address", "email", "phone", "position", "card_number", "user_id")
      .transact(db.transactor)
  }

  def update(obj: Employee): Task[Employee] = {
    sql"update employees set (full_name, address, email, phone, position, card_number, user_id) = (${obj.fullName}, ${obj.address}, ${obj.email}, ${obj.phone}, ${obj.position}, ${obj.cardNumber}, ${obj.userId}) where ID = ${obj.id}".asInstanceOf[Fragment]
      .update.withUniqueGeneratedKeys[Employee]("id", "full_name", "address", "email", "phone", "position", "card_number", "user_id")
      .transact(db.transactor)
  }

  def list(count: Int = 10000, offset: Int = 0): Task[List[Employee]] = {
    sql"SELECT * FROM employees OFFSET $offset LIMIT $count".asInstanceOf[Fragment]
      .query[Employee]
      .list
      .transact(db.transactor)
  }

  def withoutKassa: Task[List[Employee]] = {
    sql"SELECT * FROM employees LEFT OUTER JOIN kassa ON kassa.employee_id = employees.id WHERE kassa.employee_id ISNULL".asInstanceOf[Fragment]
      .query[Employee]
      .list
      .transact(db.transactor)
  }

  def withoutUser: Task[List[Employee]] = {
    sql"SELECT * FROM employees WHERE user_id ISNULL".asInstanceOf[Fragment]
      .query[Employee]
      .list
      .transact(db.transactor)
  }

  def delete(obj: Employee): Task[Int] = {
    sql"DELETE FROM employees WHERE ID = ${obj.id}".asInstanceOf[Fragment]
      .update
      .run
      .transact(db.transactor)
  }
}
