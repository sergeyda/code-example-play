package repository.implementations

import doobie.imports._
import models.Unit
import monix.eval._
import repository.UnitRepository
import services.Db

/**
  * @author Created by Sergey Dashko (sdashko@gmail.com) on 10/2/17.
  */
class UnitRepositoryImplem(val db: Db) extends UnitRepository {
    def findOne(id: Long): Task[Option[Unit]] = {
      sql"select * from units WHERE ID = $id".asInstanceOf[Fragment]
        .query[Unit]
        .option
        .transact(db.transactor)
    }

  def findOneByTitle(title: String): Task[Option[Unit]] = {
      sql"select * from units WHERE title = $title".asInstanceOf[Fragment]
        .query[Unit]
        .option
        .transact(db.transactor)
    }


    def insert(obj: Unit): Task[Unit] = {
      sql"insert into units (title) values (${obj.title})".asInstanceOf[Fragment]
        .update.withUniqueGeneratedKeys[Unit]("id", "title")
        .transact(db.transactor)
    }

    def update(obj: Unit): Task[Unit] = {
      sql"update units set (title) = (${obj.title}) where ID = ${obj.id}".asInstanceOf[Fragment]
        .update.withUniqueGeneratedKeys[Unit]("id", "title")
        .transact(db.transactor)
    }

  def list(count: Int = 10000, offset: Int = 0): Task[List[Unit]] = {
    sql"select * from units offset $offset limit $count".asInstanceOf[Fragment]
      .query[Unit]
      .list
      .transact(db.transactor)
  }

  def delete(obj: Unit): Task[Int] = {
    sql"DELETE FROM units where ID = ${obj.id}".asInstanceOf[Fragment]
      .update
      .run
      .transact(db.transactor)
  }
}
