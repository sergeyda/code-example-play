package repository

import actionbuilders.UserRequest
import doobie.imports.ConnectionIO
import helpers.LogHelper.{Added, Deleted, Updated}
import models.LoggableModel

/**
  * @author Created by Sergey Dashko (sdashko@gmail.com) on 9/23/17.
  */

trait LoggableRawRepository[T <: LoggableModel] extends HasRawOperations[T]{ this: LoggableRepository[T] =>

  def rawInsertAndLog(obj: T)(implicit rep: LogRepository, userReq: UserRequest[_]): ConnectionIO[T] = {
    for{
      model <- createInsert(obj)
      _ <- rep.createInsert(operationToLog(Added(obj)))
    } yield model
  }

  def rawUpdateAndLog(oldObj: T, newObj: T)(implicit rep: LogRepository, userReq: UserRequest[_]): ConnectionIO[T] = {
    for{
      model <- createUpdate(newObj)
      _ <- rep.createInsert(operationToLog(Updated(oldObj, newObj)))
    } yield model
  }

  def rawDeleteAndLog(obj: T)(implicit rep: LogRepository, userReq: UserRequest[_]): ConnectionIO[Int] = {
    for{
      result <- createDelete(obj)
      _ <- rep.createInsert(operationToLog(Deleted(obj)))
    } yield result
  }
}
