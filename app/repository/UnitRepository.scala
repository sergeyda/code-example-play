package repository

import models.Unit
import monix.eval.Task

/**
  * @author Created by Sergey Dashko (sdashko@gmail.com) on 10/2/17.
  */
trait UnitRepository extends Repository[Unit]  with LoggableRepository[Unit] {
  def findOneByTitle(title: String): Task[Option[Unit]]
}
