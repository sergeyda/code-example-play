package repository

import models.{User, UserFront}
import monix.eval.Task
import users._

/**
  * Created by sergey on 6/19/17.
  */
trait UserRepository extends Repository[User] with LoggableRepository[User]{
  def findOneByLogin(login: Login): Task[Option[User]]
  def findOneFront(id: Long): Task[Option[UserFront]]
  def listFront(count: Int = 10000, offset: Int = 0): Task[List[UserFront]]
}
