package repository

import doobie.imports.Fragment
import models.{Good, GoodWithUnit}
import monix.eval.Task

/**
  * Created by sergey on 6/19/17.
  */
trait GoodRepository extends Repository[Good]  with LoggableRepository[Good]{
  def findOneWithUnit(id: Long): Task[Option[GoodWithUnit]]
  def listByParent(parentId: Long): Task[List[Good]]
  def listWithUnit(count: Int = 10000, offset: Int = 0): Task[List[GoodWithUnit]]
  def search(term: String): Task[List[Good]]
  def listWithParents(where: Fragment): Task[List[Good]]
  def list(where: Fragment): Task[List[Good]]
  def searchByKeywordWhere(term: String): Fragment
}
