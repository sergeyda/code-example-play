package repository

import doobie.imports.ConnectionIO
import services.Db

/**
  * @author Created by Sergey Dashko (sdashko@gmail.com) on 12/3/17.
  */
trait HasRawOperations[T] {
  def createInsert(obj: T): ConnectionIO[T]
  def createUpdate(obj: T): ConnectionIO[T]
  def createDelete(obj: T): ConnectionIO[Int]
  val db: Db
}
