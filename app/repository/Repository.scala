package repository

import monix.eval.Task
import services.Db
/**
  * Created by sergey on 6/19/17.
  */
trait Repository[T] {
  def list(count: Int = 10000, offset: Int = 0): Task[List[T]]
  def findOne(id: Long): Task[Option[T]]
  def delete(obj: T): Task[Int]
  def update(obj: T): Task[T]
  def insert(obj: T): Task[T]
  val db: Db
}
