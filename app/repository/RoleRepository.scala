package repository

import models.Role

/**
  * Created by sergey on 6/19/17.
  */
trait RoleRepository extends Repository[Role]
