package controllers

import actionbuilders.{OnlyAuth, UserAction}
import contexts.DbExecutionContext
import helpers.ProvidesDbScheduler
import play.api.i18n._
import play.api.mvc.{AbstractController, ControllerComponents, RequestHeader}
import repository.UserRepository
import services.Mailer


class JsHelperController(cc: ControllerComponents, rep: UserRepository, val dbEC: DbExecutionContext,
                         val mailer: Mailer, langs: Langs, configuration: play.api.Configuration)(implicit val assetsFind: AssetsFinder, webJarAssets: WebJarAssets) extends AbstractController(cc)
  with I18nSupport with ProvidesDbScheduler{

  implicit lazy val executionContext = defaultExecutionContext


  def datatableLangFileUrl = (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth) { implicit request =>
    val currentLang = request2Messages.lang.language
    val langFileName = configuration.getOptional[String](s"datatables.lang.files.$currentLang")
    val langFilePath = langFileName.map( file => assetsFind.path(s"scripts/$file")).getOrElse("")
    Ok(langFilePath)
  }


}
