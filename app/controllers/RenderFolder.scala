package controllers

import actionbuilders.{UserAction, UserRequest}
import play.api.i18n.I18nSupport
import play.api.mvc.{AbstractController, Call, Result}
import repository.{LogRepository, LoggableRepository, Repository, UserRepository}
import services.Mailer
import actionbuilders._
import cats.data.OptionT
import helpers.ProvidesDbScheduler
import models.{LoggableModel, TreeStructureModel}
import validators.Folder
import validators.Folder._

import scala.concurrent.ExecutionContext
import monix.cats._
import monix.eval.Task
import play.api.libs.json.Json

/**
  * @author Created by Sergey Dashko (sdashko@gmail.com) on 10/6/17.
  */
trait RenderFolder[T <: TreeStructureModel with LoggableModel] { this: AbstractController with I18nSupport with ProvidesDbScheduler with RenderError =>
  val mailer: Mailer
  val rep: UserRepository
  val doAddFolderRoute: Call
  val doEditFolderRoute: Call
  val listFolderRoute: Call
  val listFolderTitle: String
  val editFolderTitle: String
  def addFolder(validator: Folder)(implicit userRequest: UserRequest[_]): Task[Result]
  val repository: Repository[T] with LoggableRepository[T]
  implicit val logRep: LogRepository
  implicit val assetsFind: AssetsFinder
  implicit val webJarAssets: WebJarAssets
  implicit lazy val executionContext: ExecutionContext = defaultExecutionContext

  val checkParent: (Long) => Task[Boolean] = (parenId: Long) =>
    OptionT(repository.findOne(parenId))
      .map(_ => true)
      .getOrElse(false)


  def renderAddFolder = (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth andThen OnlyAllowed) { implicit request: UserRequest[_] =>
    Ok(views.html.addFolderForm(folderForm, doAddFolderRoute))
  }

  def renderEditFolder(id: Long) = (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth andThen OnlyAllowed).async { implicit request: UserRequest[_] =>
    val result = OptionT(repository.findOne(id)).map{ obj =>
      val folder = Folder(
        title = obj.title,
        id = Some(obj.id),
        parentId = obj.parentId
      )
      Ok(views.html.editFolder(folderForm.fill(folder), doEditFolderRoute, listFolderRoute, listFolderTitle, editFolderTitle))
    }.getOrElse{
      NotFound(views.html.err404(None))
    }
    finishWithRenderError(result, scheduler)
  }

  def doEditFolder = (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth andThen OnlyAllowed).async { implicit request: UserRequest[_] =>
    val result = folderForm.bindFromRequest().fold(
      invalid => {
        Task(Ok(views.html.editFolder(invalid, doEditFolderRoute, listFolderRoute, listFolderTitle, editFolderTitle)))
      },
      valid =>{
        OptionT(repository.findOne(valid.id.get)).flatMap { obj =>
          val newObject = obj.updateTitle(valid.title).asInstanceOf[T]
          OptionT.liftF(repository.updateAndLog(obj, newObject)).map { _ =>
            Redirect(listFolderRoute)
          }
        }.getOrElse(NotFound(views.html.err404(None)))
      })
    finishWithRenderError(result, scheduler)
  }

  def doAddFolder = (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth andThen OnlyAllowed).async { implicit request: UserRequest[_] =>
    val result = folderForm.bindFromRequest().fold(
      invalid => {
        Task(Ok(Json.toJson(Json.obj("result" -> "error", "form" -> views.html.addFolderForm(invalid, doAddFolderRoute).body))))
      },
      valid => {
        (valid.id, valid.parentId) match {
          case (None, Some(parentId)) =>
            for {
              checkResult <- checkParent(parentId)
              addedResult <- if (checkResult) addFolder(valid) else Task(NotFound("parent not found"))
            } yield addedResult
          case (None, None) =>
            addFolder(valid)
          case (_, _) => Task(InternalServerError(views.html.err500()))
        }
      }

    )
    result.runAsync(scheduler)
  }
}
