package controllers

import actionbuilders._
import cats.data.OptionT
import contexts.DbExecutionContext
import helpers.ProvidesDbScheduler
import models.Unit
import monix.eval.Task
import play.api.data.Form
import play.api.i18n._
import play.api.mvc.{AbstractController, ControllerComponents, Result}
import repository.{LogRepository, UnitRepository, UserRepository}
import services.Mailer
import play.api.data.Forms._
import cats.implicits._
import monix.cats._
import play.api.data.validation._
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

case class UnitValidator(title: String)

class UnitsController(cc: ControllerComponents,
                      unitRep: UnitRepository,
                      val dbEC: DbExecutionContext,
                      val mailer: Mailer,
                      langs: Langs,
                      userRep: UserRepository)(
    implicit val assetsFind: AssetsFinder,
    webJarAssets: WebJarAssets,
    logRep: LogRepository)
    extends AbstractController(cc)
    with I18nSupport
    with ProvidesDbScheduler
    with RenderError {

  implicit lazy val executionContext = defaultExecutionContext

  val unitNonUniqueConstraint: Constraint[String] =
    Constraint("constraint.title.should.be.unique")({ title =>
      val busy: Task[ValidationResult] = OptionT(unitRep.findOneByTitle(title))
        .map { _ =>
          val v: ValidationResult =
            Invalid(Seq(ValidationError("unit.not.unique")))
          v
        }
        .getOrElse(Valid)
      val future = busy.runAsync(scheduler)
      Await.result(future, 3.seconds)
    })

  val form: Form[UnitValidator] = Form(
    mapping("title" -> nonEmptyText.verifying(unitNonUniqueConstraint))(
      UnitValidator.apply)(UnitValidator.unapply))

  def list =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val units: Task[List[Unit]] = unitRep.list()
        val r = units.map { units =>
          Ok(views.html.units.list(units))
        }
        finishWithRenderError(r, scheduler)
      }

  def add =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed) {
      implicit request: UserRequest[_] =>
        Ok(views.html.units.add(form))
    }

  def delete(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r: Task[Result] =
          OptionT(unitRep.findOne(id))
            .flatMap { b =>
              OptionT.liftF(unitRep.deleteAndLog(b)).map { _ =>
                Redirect(routes.UnitsController.list())
              }
            }
            .getOrElse(NotFound(Messages("Unit.not.found")))

        finishWithRenderError(r, scheduler)
      }

  def edit(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = OptionT(unitRep.findOne(id))
          .map { bus =>
            val validator = UnitValidator(
              title = bus.title
            )
            Ok(views.html.units.edit(form.fill(validator), bus))
          }
          .getOrElse(
            NotFound(views.html.err404(Some(Messages("Unit.not.found")))))
        finishWithRenderError(r, scheduler)
      }

  def doEdit(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = form.bindFromRequest.fold(
          formWithErrors => {
            OptionT(unitRep.findOne(id))
              .map { bus =>
                BadRequest(views.html.units.edit(formWithErrors, bus))
              }
              .getOrElse(
                NotFound(views.html.err404(Some(Messages("Unit.not.found")))))
          },
          busData => {
            OptionT(unitRep.findOne(id))
              .flatMap { bus =>
                val newBus = bus.copy(title = busData.title)
                OptionT.liftF(unitRep.updateAndLog(bus, newBus)).map { res =>
                  Redirect(routes.UnitsController.list())
                }
              }
              .getOrElse(BadRequest(Messages("Unit.not.found")))
          }
        )
        finishWithRenderError(r, scheduler)
      }

  def doAdd =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        form.bindFromRequest.fold(
          formWithErrors => {
            Future.successful(BadRequest(views.html.units.add(formWithErrors)))
          },
          unitValidator => {
            val newUnit = Unit(
              title = unitValidator.title
            )
            val r = unitRep.insertAndLog(newUnit).map { _ =>
              Redirect(routes.UnitsController.list())
            }
            finishWithRenderError(r, scheduler)
          }
        )
      }

}
