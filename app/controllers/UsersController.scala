package controllers

import actionbuilders._
import cats.data.OptionT
import contexts.DbExecutionContext
import helpers.ProvidesDbScheduler
import models.{Employee, Role, User, UserFront}
import monix.eval.Task
import play.api.data.Form
import play.api.i18n._
import play.api.mvc.{AbstractController, ControllerComponents, Result}
import repository.{
  EmployeeRepository,
  LogRepository,
  RoleRepository,
  UserRepository
}
import services.{Mailer, UserService}
import play.api.data.Forms._
import cats.implicits._
import monix.cats._
import constraints._
import play.api.data.validation._

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success}

case class UserValidator(login: String,
                         name: String,
                         password: String,
                         roleId: Long)

case class UserEditValidator(login: String,
                             name: String,
                             password: Option[String],
                             roleId: Long)

case class LinkEmployer(employeeId: Option[Long])

class UsersController(cc: ControllerComponents,
                      val dbEC: DbExecutionContext,
                      val mailer: Mailer,
                      langs: Langs,
                      roleRep: RoleRepository)(
    implicit val assetsFind: AssetsFinder,
    webJarAssets: WebJarAssets,
    logRep: LogRepository,
    userRep: UserRepository,
    emplRep: EmployeeRepository)
    extends AbstractController(cc)
    with I18nSupport
    with ProvidesDbScheduler
    with RenderError {

  implicit lazy val executionContext = defaultExecutionContext

  def loginBusyConstraint(userId: Option[Long]): Constraint[String] =
    Constraint("constraint.should.be.unique")({ login =>
      val busy: Task[ValidationResult] = OptionT(userRep.findOneByLogin(login))
        .map { user =>
          userId
            .map { usrId =>
              if (usrId != user.id)
                Invalid(Seq(ValidationError("Login.not.unique")))
              else
                Valid
            }
            .getOrElse(
              Invalid(Seq(ValidationError("Login.not.unique")))
            )
        }
        .getOrElse(Valid)
      val future = busy.runAsync(scheduler)
      Await.result(future, 3.seconds)
    })

  val addForm: Form[UserValidator] = Form(
    mapping(
      "login" -> nonEmptyText.verifying(loginBusyConstraint(None)),
      "name" -> nonEmptyText,
      "password" -> nonEmptyText(6, 10).verifying(passwordCheckConstraint),
      "roleId" -> longNumber
    )(UserValidator.apply)(UserValidator.unapply))

  def editForm(userId: Long): Form[UserEditValidator] =
    Form(
      mapping(
        "login" -> nonEmptyText.verifying(loginBusyConstraint(Some(userId))),
        "name" -> nonEmptyText,
        "password" -> optional(nonEmptyText(6, 10).verifying(passwordCheckConstraint)),
        "roleId" -> longNumber)(UserEditValidator.apply)(
        UserEditValidator.unapply))

  val linkEmployerForm: Form[LinkEmployer] = Form(
    mapping(
      "employeeId" -> optional(longNumber)
    )(LinkEmployer.apply)(LinkEmployer.unapply))

  def list =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = userRep.listFront().map { usl =>
          Ok(views.html.users.list(usl))
        }
        finishWithRenderError(r, scheduler)
      }

  def add =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = roleRep.list().map { rolel =>
          Ok(views.html.users.add(addForm, rolel))
        }
        finishWithRenderError(r, scheduler)
      }

  def delete(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r: Task[Result] = {
          OptionT(userRep.findOne(id))
            .flatMap { u =>
              OptionT.liftF(userRep.deleteAndLog(u)).map { _ =>
                Redirect(routes.UsersController.list())
              }
            }
            .getOrElse(NotFound(Messages("User not found")))
        }

        finishWithRenderError(r, scheduler)
      }

  def edit(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = findUserAndRoles(id).map {
          case (Some(u), roles) =>
            val validator = UserEditValidator(
              name = u.name,
              roleId = u.roleId,
              login = u.login,
              password = None
            )
            Ok(views.html.users.edit(editForm(id).fill(validator), roles, u))
          case (None, _) =>
            BadRequest(Messages("User.not.found"))
        }
        finishWithRenderError(r, scheduler)
      }

  def linkEmployee(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = renderLinkForm(id)
          .getOrElse(
            BadRequest(Messages("User.not.found"))
          )
        finishWithRenderError(r, scheduler)
      }

  def doLinkEmployee(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = linkEmployerForm.bindFromRequest.fold(
          formWithErrors => {
            renderLinkForm(id)
              .getOrElse(
                Redirect(routes.UsersController.list())
              )
          },
          validator => {
            val updatedEmplTask: Task[Option[Employee]] =
              validator.employeeId
                .map { empId =>
                  for {
                    _ <- UserService.unLinkEmployee(id)
                    l <- UserService.linkEmployee(id, empId)
                  } yield l
                }
                .getOrElse {
                  UserService.unLinkEmployee(id)
                }

            updatedEmplTask.map { _ =>
              Redirect(routes.UsersController.list())
            }
          }
        )
        finishWithRenderError(r, scheduler)
      }

  def doEdit(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = editForm(id).bindFromRequest.fold(
          formWithErrors => {
            findUserAndRoles(id).map {
              case (Some(u), roles) =>
                BadRequest(views.html.users.edit(formWithErrors, roles, u))
              case (None, _) => BadRequest(Messages("User.not.found"))
            }
          },
          userData => {
            OptionT(userRep.findOne(id))
              .flatMap {
                u =>
                  val updatedUser: Task[User] =
                    userData.password
                      .map { pass =>
                        for {
                          hash <- Task.fromTry(UserService.passwordHash(pass))
                        } yield {
                          u.copy(
                            passwordHash = hash,
                            login = userData.login,
                            name = userData.name,
                            roleId = userData.roleId
                          )
                        }
                      }
                      .getOrElse {
                        Task.now(
                          u.copy(
                            login = userData.login,
                            name = userData.name,
                            roleId = userData.roleId
                          ))
                      }

                  val savedUser = for {
                    uu <- updatedUser
                    _ <- userRep.updateAndLog(u, uu)
                  } yield Redirect(routes.UsersController.list())
                  OptionT.liftF(savedUser)
              }
              .getOrElse(
                BadRequest(Messages("User.not.found"))
              )
          }
        )
        finishWithRenderError(r, scheduler)
      }

  def doAdd =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = addForm.bindFromRequest.fold(
          formWithErrors => {
            roleRep.list().map { rolel =>
              BadRequest(views.html.users.add(formWithErrors, rolel))
            }
          },
          userData => {
            val passHash: Task[String] =
              Task.fromTry(UserService.passwordHash(userData.password))

            passHash.flatMap { hash =>
              val newUser = User(
                login = userData.login,
                passwordHash = hash,
                name = userData.name,
                roleId = userData.roleId,
                session = None
              )

              userRep.insertAndLog(newUser).map { _ =>
                Redirect(routes.UsersController.list())
              }
            }
          }
        )
        finishWithRenderError(r, scheduler)
      }

  private def findUserAndRoles(id: Long): Task[(Option[User], List[Role])] = {
    val user = userRep.findOne(id)
    val roles = roleRep.list()
    Task.zip2(user, roles)
  }

  private def renderLinkForm(
                             userId: Long)(implicit request: UserRequest[_]): OptionT[Task, Result] = {
    for {
      user <- OptionT(userRep.findOne(userId))
      employees <- OptionT.liftF(emplRep.withoutUser)
      currentEmployee <- OptionT.liftF(emplRep.findOneByUserId(userId))
    } yield {
      val validator = LinkEmployer(currentEmployee.map(_.id))
      val empls = currentEmployee.map(e => employees :+ e).getOrElse(employees)
      Ok(views.html.users.link_employer(linkEmployerForm.fill(validator), empls, user))
    }
  }
}
