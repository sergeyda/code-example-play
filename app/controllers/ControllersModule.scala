package controllers

import com.softwaremill.macwire._
import contexts.DbExecutionContextImpl
import play.api.BuiltInComponentsFromContext
import play.api.i18n.I18nComponents
import repository.RepositoryModule
import services.ServicesModule

/**
  * Created by sergey on 6/19/17.
  */
trait ControllersModule{ this:  AssetsComponents with I18nComponents
  with BuiltInComponentsFromContext with ServicesModule
  with RepositoryModule =>


  implicit lazy val assFinder: AssetsFinder = assetsFinder
  implicit lazy val dbEC = wire[DbExecutionContextImpl]

  implicit lazy val webJarController: WebJarAssets = wire[WebJarAssets]
  lazy val authController: AuthController = wire[AuthController]
  lazy val homeController: HomeController = wire[HomeController]
  lazy val usersController: UsersController = wire[UsersController]
  lazy val goodsController: GoodsController = wire[GoodsController]
  lazy val empController: EmployeesController = wire[EmployeesController]
  lazy val unitController: UnitsController = wire[UnitsController]
  lazy val jsHelperController: JsHelperController = wire[JsHelperController]
  lazy val jsRoutesController: JavascriptRoutesController = wire[JavascriptRoutesController]
}
