package controllers

import actionbuilders._
import cats.data.OptionT
import contexts.DbExecutionContext
import helpers.ProvidesDbScheduler
import models.Employee
import monix.eval.Task
import play.api.data.Form
import play.api.i18n._
import play.api.mvc.{AbstractController, ControllerComponents, Result}
import repository.{EmployeeRepository, LogRepository, UserRepository}
import services.Mailer
import play.api.data.Forms._
import cats.implicits._
import monix.cats._

import scala.concurrent.Future

case class EmployeeValidator(fullName: String,
                             address: Option[String],
                             email: Option[String],
                             phone: Option[String],
                             position: Option[String],
                             cardNumber: Option[String])

class EmployeesController(cc: ControllerComponents,
                          empRep: EmployeeRepository,
                          val dbEC: DbExecutionContext,
                          val mailer: Mailer,
                          langs: Langs,
                          userRep: UserRepository)(
    implicit val assetsFind: AssetsFinder,
    webJarAssets: WebJarAssets,
    logRep: LogRepository)
    extends AbstractController(cc)
    with I18nSupport
    with ProvidesDbScheduler
    with RenderError {

  implicit lazy val executionContext = defaultExecutionContext

  val form: Form[EmployeeValidator] = Form(
    mapping(
      "fullName" -> nonEmptyText,
      "address" -> optional(text),
      "email" -> optional(email),
      "phone" -> optional(text),
      "position" -> optional(text),
      "cardNumber" -> optional(text)
    )(EmployeeValidator.apply)(EmployeeValidator.unapply))

  def list =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = empRep.list().map { es =>
          Ok(views.html.employees.list(es))
        }
        finishWithRenderError(r, scheduler)
      }

  def add =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed) {
      implicit request: UserRequest[_] =>
        Ok(views.html.employees.add(form))
    }

  def delete(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r: Task[Result] =
          (for {
            e <- OptionT(empRep.findOne(id))
            _ <- OptionT.liftF(empRep.deleteAndLog(e))
          } yield Redirect(routes.EmployeesController.list()))
            .getOrElse(NotFound(Messages("Employee.not.found")))

        finishWithRenderError(r, scheduler)
      }

  def edit(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = OptionT(empRep.findOne(id))
          .map { e =>
            val validator = EmployeeValidator(
              fullName = e.fullName,
              address = e.address,
              email = e.email,
              phone = e.phone,
              position = e.position,
              cardNumber = e.cardNumber
            )
            Ok(views.html.employees.edit(form.fill(validator), e))
          }
          .getOrElse(BadRequest(Messages("Employee.not.found")))
        finishWithRenderError(r, scheduler)
      }

  def doEdit(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = form.bindFromRequest.fold(
          formWithErrors => {
            OptionT(empRep.findOne(id))
              .map { employee =>
                BadRequest(views.html.employees.edit(formWithErrors, employee))
              }
              .getOrElse(BadRequest(Messages("Employee.not.found")))
          },
          validator => {
            OptionT(empRep.findOne(id))
              .flatMap {
                employee =>
                  val newEmployee = employee.copy(
                    fullName = validator.fullName,
                    address = validator.address,
                    email = validator.email,
                    phone = validator.phone,
                    position = validator.position,
                    cardNumber = validator.cardNumber
                  )
                  OptionT.liftF(
                    empRep.updateAndLog(employee, newEmployee).map { _ =>
                      Redirect(routes.EmployeesController.list())
                    }
                  )
              }
              .getOrElse(BadRequest(Messages("Employee.not.found")))

          }
        )
        finishWithRenderError(r, scheduler)
      }

  def doAdd =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        form.bindFromRequest.fold(
          formWithErrors => {
            Future.successful(
              BadRequest(views.html.employees.add(formWithErrors)))
          },
          validator => {
            val employee = Employee(
              fullName = validator.fullName,
              address = validator.address,
              email = validator.email,
              phone = validator.phone,
              position = validator.position,
              cardNumber = validator.cardNumber
            )
            val r = empRep.insertAndLog(employee).map { _ =>
              Redirect(routes.EmployeesController.list())
            }
            finishWithRenderError(r, scheduler)
          }
        )
      }
}
