package controllers

import actionbuilders.{OnlyAuth, UserAction, UserRequest}
import contexts.DbExecutionContext
import helpers.ProvidesDbScheduler
import play.api.i18n._
import play.api.mvc.{AbstractController, ControllerComponents}
import repository.UserRepository
import services.Mailer



class HomeController(cc: ControllerComponents, rep: UserRepository, val dbEC: DbExecutionContext,
                     val mailer: Mailer, langs: Langs)(implicit val assetsFind: AssetsFinder, webJarAssets: WebJarAssets) extends AbstractController(cc)
  with I18nSupport with ProvidesDbScheduler{

  implicit lazy val executionContext = defaultExecutionContext


  def home = (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth) { implicit request: UserRequest[_] =>
    Ok(views.html.home("Home"))
  }


}
