package controllers

import models.{Log, User, UserFront}
import play.api.data.Form
import play.api.i18n._
import play.api.mvc._
import play.api.data.Forms._
import cats.implicits._
import monix.eval._
import monix.cats._
import contexts.DbExecutionContext
import helpers.ProvidesDbScheduler
import actionbuilders._
import cats.data.OptionT
import repository.{LogRepository, UserRepository}
import services.{Db, Mailer, UserService}


case class LoginValidator(login: String, password: String)

class AuthController(langs: Langs,
                     cc: ControllerComponents,
                     val db: Db,
                     val userRep: UserRepository,
                     val dbEC: DbExecutionContext,
                     val mailer: Mailer,
                     logRep: LogRepository)(
    implicit val assetsFind: AssetsFinder,
    webJarAssets: WebJarAssets)
    extends AbstractController(cc)
    with I18nSupport
    with ProvidesDbScheduler
    with RenderError {

  implicit lazy val executionContext = defaultExecutionContext
  val loginForm: Form[LoginValidator] = Form(
    mapping("login" -> nonEmptyText, "password" -> nonEmptyText)(
      LoginValidator.apply)(LoginValidator.unapply))

  def doLogin() =
    Action.async { implicit request: Request[_] =>
      val r = loginForm.bindFromRequest.fold(
        formWithErrors => Task(BadRequest(views.html.login(formWithErrors))),
        value => {
          {
            for {
              user <- OptionT(userRep.findOneByLogin(value.login))
              checkResult <- OptionT.liftF(
                Task.fromTry(
                  UserService.checkPassword(value.password, user.passwordHash))
              )
            } yield {
              if (checkResult)
                authenticateUser(user)
              else
                Task.now(
                  Ok(views.html.login(
                    loginForm.withGlobalError(Messages("incorrect.data")))))
            }
          }.getOrElse(
              Task.now(Ok(views.html.login(
                loginForm.withGlobalError(Messages("incorrect.data")))))
            )
            .flatten
        }
      )
      finishWithRenderError(r, scheduler)
    }

  def login = (Action andThen OnlyNotAuth(executionContext)) {
    implicit request: RequestHeader =>
      Ok(views.html.login(loginForm))
  }

  def logout =
    (UserAction(parse.defaultBodyParser)(scheduler, userRep) andThen OnlyAuth)
      .async { implicit request: UserRequest[_] =>
        val r = request.user
          .map { user: UserFront =>
            val log = Log(`type` = "log.User.logged.out",
                          text = "{\"arguments\":[]}",
                          userId = user.id,
                          hidden = false)
            logRep.insert(log).map { _ =>
              Redirect(routes.AuthController.login()).withNewSession
            }
          }
          .getOrElse {
            Task.now(Forbidden)
          }
        finishWithRenderError(r, scheduler)
      }

  private def authenticateUser(user: User)(
      implicit request: RequestHeader): Task[Result] = {
    for {
      user <- userRep.update(
        user.copy(session = Some(UserService.sessionToken)))
      log = Log(`type` = "log.User.logged.in",
                text = "{\"arguments\":[]}",
                userId = user.id,
                hidden = false)
      _ <- logRep.insert(log)
    } yield
      Redirect("/").withSession("user" -> user.id.toString,
                                "token" -> user.session.getOrElse("nosession"))
  }

}
