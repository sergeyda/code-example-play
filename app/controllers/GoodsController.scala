package controllers

import actionbuilders._
import helpers.FlattenTree._
import cats.data.OptionT
import contexts.DbExecutionContext
import helpers._
import models.{Good, GoodWithUnit}
import monix.cats._
import monix.eval.Task
import play.api.data.Form
import play.api.data.Forms.{longNumber, mapping, nonEmptyText, optional, text}
import play.api.i18n._
import play.api.libs.json._
import play.api.mvc.{AbstractController, Call, ControllerComponents, Result}
import repository.{
  GoodRepository,
  LogRepository,
  UnitRepository,
  UserRepository
}
import services.Mailer
import validators.Folder
import validators.Folder._

case class GoodFormClass(title: String,
                         id: Option[Long],
                         parentId: Long,
                         unitId: Long,
                         barcode: Option[String],
                         code: Option[String])

class GoodsController(cc: ControllerComponents,
                      val rep: UserRepository,
                      val repository: GoodRepository,
                      unitRep: UnitRepository,
                      val dbEC: DbExecutionContext,
                      val mailer: Mailer,
                      langs: Langs)(implicit val assetsFind: AssetsFinder,
                                    val webJarAssets: WebJarAssets,
                                    val logRep: LogRepository)
    extends AbstractController(cc)
    with I18nSupport
    with ProvidesDbScheduler
    with RenderError
    with RenderFolder[Good] {

  val doAddFolderRoute: Call = routes.GoodsController.doAddFolder()
  val doEditFolderRoute: Call = routes.GoodsController.doEditFolder()
  val listFolderRoute: Call = routes.GoodsController.list()
  val listFolderTitle = "Goods"
  val editFolderTitle = "Edit.good"

  implicit val goodWithUnitWrites: Writes[GoodWithUnit] =
    (good: GoodWithUnit) =>
      Json.obj(
        "title" -> JsString(good.title),
        "id" -> JsString(good.id.toString),
        "key" -> JsString(good.id.toString),
        "unit" -> JsString(good.unit.getOrElse("")),
        "code" -> JsString(good.code.getOrElse("")),
        "barcode" -> JsString(good.barcode.getOrElse("")),
        "folder" -> JsBoolean(false),
        "checkbox" -> JsBoolean(false)
    )

  val goodForm: Form[GoodFormClass] = Form(
    mapping(
      "title" -> nonEmptyText,
      "id" -> optional(longNumber),
      "parentId" -> longNumber,
      "unitId" -> longNumber,
      "barcode" -> optional(text),
      "code" -> optional(text)
    )(GoodFormClass.apply)(GoodFormClass.unapply))

  def addFolder(validator: Folder)(
      implicit userRequest: UserRequest[_]): Task[Result] = {
    val good = Good(
      title = validator.title,
      parentId = validator.parentId,
      unitId = None,
      barcode = None,
      code = None,
      folder = true
    )
    repository.insertAndLog(good).map { newNode =>
      Ok(
        Json.toJson(
          Json.obj("result" -> "success",
                   "node" -> Folder(
                     id = Some(newNode.id),
                     parentId = newNode.parentId,
                     title = newNode.title
                   ))))
    }
  }

  def list =
    (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth andThen OnlyAllowed) {
      implicit request: UserRequest[_] =>
        Ok(views.html.goods.list())
    }

  def renderAddForm =
    (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val units = unitRep.list()
        val r = units.map { list =>
          Ok(views.html.goods.addGoodForm(goodForm, list))
        }
        finishWithRenderError(r, scheduler)
      }

  def edit(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val unitsT: Task[List[models.Unit]] = unitRep.list()
        val goodT: Task[Option[Good]] = repository.findOne(id)
        val successResult: OptionT[Task, Result] = for {
          good <- OptionT(goodT)
          units <- OptionT.liftF(unitsT)
          if good.parentId.nonEmpty && !good.folder
        } yield {
          val form = goodForm.fill(
            GoodFormClass(
              title = good.title,
              code = good.code,
              barcode = good.barcode,
              unitId = good.unitId.get,
              parentId = good.parentId.get,
              id = Some(good.id)
            ))
          Ok(views.html.goods.edit(form, units))
        }

        finishWithRenderError(successResult.getOrElse(NotFound), scheduler)
      }

  def tree =
    (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val goods = repository.listWithUnit()
        val r = goods.map { list =>
          Ok(Json.toJson(GoodsTree(list)))
        }
        finishWithRenderError(r, scheduler)
      }

  def search(term: Option[String]) =
    (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val r = term
          .map { t =>
            val goods =
              repository.listWithParents(repository.searchByKeywordWhere(t))
            goods.map { list =>
              Ok(Json.toJson(Map("results" -> FlattenTree(list))))
            }
          }
          .getOrElse(Task.now(Ok(Json.toJson(Map("results" -> JsArray())))))

        finishWithRenderError(r, scheduler)
      }

  def delete(id: Long) =
    (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val canBeDeleted: Task[Boolean] = for {
          children <- repository.listByParent(id)
        } yield if (children.isEmpty) true else false

        val result: Task[Result] = for {
          check <- canBeDeleted
          rslt <- if (check) {
            OptionT(repository.findOne(id))
              .flatMap { good =>
                OptionT.liftF(repository.deleteAndLog(good)).map { _ =>
                  Ok(Json.toJson(Json.obj("result" -> "success")))
                }
              }
              .getOrElse(NotFound("Good not found"))
          } else {
            Task(BadRequest)
          }
        } yield rslt

        result.runAsync(scheduler)
      }

  def doEditGood =
    (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val update = (validator: GoodFormClass, id: Long) =>
          OptionT(repository.findOne(id))
            .flatMap { good =>
              val newGood = good.copy(
                title = validator.title,
                unitId = Some(validator.unitId),
                code = validator.code,
                barcode = validator.barcode
              )
              OptionT.liftF(repository.updateAndLog(good, newGood)).map { _ =>
                Redirect(routes.GoodsController.list())
              }
            }
            .getOrElse(NotFound(views.html.err404(None)))

        val r = goodForm
          .bindFromRequest()
          .fold(
            invalid => {
              val units = unitRep.list()
              units.map { list =>
                Ok(views.html.goods.edit(invalid, list))
              }
            },
            valid => {
              valid.id
                .map { existingId =>
                  for {
                    checkResult <- checkParent(valid.parentId)
                    updateResult <- if (checkResult) update(valid, existingId)
                    else Task(NotFound("parent not found"))
                  } yield updateResult
                }
                .getOrElse {
                  Task(InternalServerError(views.html.err500()))
                }
            }
          )
        finishWithRenderError(r, scheduler)
      }

  def doAddGood =
    (UserAction(parse.defaultBodyParser)(scheduler, rep) andThen OnlyAuth andThen OnlyAllowed)
      .async { implicit request: UserRequest[_] =>
        val add: (GoodFormClass) => Task[Result] =
          (validator: GoodFormClass) => {
            val good = Good(
              title = validator.title,
              parentId = Some(validator.parentId),
              unitId = Some(validator.unitId),
              barcode = validator.barcode,
              code = validator.code,
              folder = false
            )

            for {
              newGood <- repository.insertAndLog(good)
              mbGoodWithUnit <- repository.findOneWithUnit(newGood.id)
            } yield
              Ok(Json.toJson(
                Json.obj("result" -> "success", "node" -> mbGoodWithUnit.get)))
          }

        val r = goodForm
          .bindFromRequest()
          .fold(
            invalid => {
              val units = unitRep.list()
              units.map { list =>
                Ok(Json.toJson(Json.obj(
                  "result" -> "error",
                  "form" -> views.html.goods.addGoodForm(invalid, list).body)))
              }
            },
            valid => {
              valid.id
                .map { _ =>
                  Task(InternalServerError(views.html.err500()))
                }
                .getOrElse {
                  for {
                    checkResult <- checkParent(valid.parentId)
                    addedResult <- if (checkResult) add(valid)
                    else Task(NotFound("parent not found"))
                  } yield addedResult
                }
            }
          )
        finishWithRenderError(r, scheduler)
      }

}
