package controllers

/**
  * Created by sergey on 6/28/17.
  */
import play.api.mvc.{AbstractController, ControllerComponents}
import play.api.routing.JavaScriptReverseRouter


class JavascriptRoutesController(cc: ControllerComponents) extends AbstractController(cc) {

  def get() = Action {
    implicit request =>
      Ok(
        JavaScriptReverseRouter("jsRoutes")(
          routes.javascript.JsHelperController.datatableLangFileUrl,
          routes.javascript.GoodsController.tree,
          routes.javascript.GoodsController.renderAddFolder,
          routes.javascript.GoodsController.renderAddForm,
          routes.javascript.GoodsController.delete,
          routes.javascript.GoodsController.doAddGood,
          routes.javascript.GoodsController.edit,
          routes.javascript.GoodsController.renderEditFolder,
          routes.javascript.GoodsController.search
        )
      ).as("text/javascript")
  }


}