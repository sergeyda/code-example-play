package controllers

import monix.eval.Task
import monix.execution.{CancelableFuture, Scheduler}
import play.api.Logger
import play.api.i18n.I18nSupport
import play.api.mvc.{AbstractController, Request, Result}
import services.Mailer


/**
  * Created by sergey on 6/24/17.
  */
trait RenderError {
  this: I18nSupport with AbstractController =>
  val mailer: Mailer

  def renderError(e: Throwable)(implicit request: play.api.mvc.RequestHeader, webJarAssets: WebJarAssets, assetsFind: AssetsFinder): Task[Result] = {
    Logger.error("There was an error: " + e.toString)
    Logger.error("Stack trace: ")
    e.getStackTrace.foreach{ el =>
      Logger.error(el.toString)
    }
      try {
        mailer.sendErrorToAdmin(e)
      } catch {
        case _ : Throwable => Logger.error("Can't send email with error to admin")
      }
    Task.now{ InternalServerError(views.html.err500()) }
  }

  def finishWithRenderError(
                             result: Task[Result],
                             scheduler: Scheduler
                           )
                           (
                             implicit request: Request[_],
                             assetsFind: AssetsFinder,
                             webJarAssets: WebJarAssets
                           ): CancelableFuture[Result] = {
    result.onErrorRecoverWith {
      case e =>
        renderError(e)
    }.runAsync(scheduler)
  }
}
