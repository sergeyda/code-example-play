package validators


import play.api.data.Form
import play.api.data.Forms.{longNumber, mapping, nonEmptyText, optional}
import play.api.libs.json._


case class Folder(title: String, id: Option[Long], parentId: Option[Long])

case object Folder {
  implicit val folderWrites = new Writes[Folder] {
  def writes(node: Folder) =
    Json.obj(
      "title" -> JsString(node.title),
      "key" -> JsNumber(node.id.getOrElse(throw new Exception(s"No id for folder: $node"))),
      "id" -> JsNumber(node.id.getOrElse(throw new Exception(s"No id for folder: $node"))),
      "folder" -> JsBoolean(true),
      "checkbox" -> JsBoolean(true)
    )
  }


  val folderForm: Form[Folder] = Form(
    mapping(
      "title" -> nonEmptyText,
      "id" -> optional(longNumber),
      "parentId" -> optional(longNumber))
    (Folder.apply)(Folder.unapply))
}
