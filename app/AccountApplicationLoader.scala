
import _root_.controllers.{AssetsComponents, ControllersModule}
import com.softwaremill.macwire._
import play.api.{Application, ApplicationLoader, BuiltInComponentsFromContext, LoggerConfigurator}
import play.api.ApplicationLoader.Context
import play.api.db.{DBComponents, HikariCPComponents}
import play.api.db.evolutions.EvolutionsComponents
import services.ServicesModule
import play.api.i18n._
import play.api.libs.mailer.MailerComponents
import play.api.routing.Router
import repository.RepositoryModule
import router.Routes

/**
  * Application loader that wires up the application dependencies using Macwire
  */
class AccountApplicationLoader extends ApplicationLoader {
  def load(context: Context): Application = new AccountComponents(context).application
}

class AccountComponents(context: Context) extends BuiltInComponentsFromContext(context)
  with AssetsComponents
  with I18nComponents
  with play.filters.HttpFiltersComponents
  with DBComponents
  with EvolutionsComponents
  with HikariCPComponents
  with MailerComponents
  with ServicesModule with RepositoryModule with ControllersModule{

  // set up logger
  LoggerConfigurator(context.environment.classLoader).foreach {
    _.configure(context.environment, context.initialConfiguration, Map.empty)
  }

  applicationEvolutions

  lazy val router: Router = {
//    // add the prefix string in local scope for the Routes constructor
    val prefix: String = "/"
    wire[Routes]
  }

}
