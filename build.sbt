name := """account"""

version := "1.0"


import sbt.Project.projectToRef

lazy val scalaV = "2.12.4"

lazy val root = (project in file(".")).settings(
  scalaVersion := scalaV,
  scalaJSProjects := jsProjects,
  pipelineStages in Assets := Seq(scalaJSPipeline),
  pipelineStages := Seq(digest, gzip),
  routesGenerator := InjectedRoutesGenerator,
  scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature"),
  libraryDependencies ++= Seq(
    jdbc,
    //    cache,
    evolutions,
    filters,
    "io.monix" %% "monix-eval" % "2.3.0",
    "io.monix" %% "monix-cats" % "2.3.0",
    "com.typesafe.play" % "play-mailer_2.12" % "6.0.0-M1",
    "org.scalatest" % "scalatest_2.12" % "3.0.3" % "test",
    "com.vmunier" % "scalajs-scripts_2.12" % "1.1.1",
    "org.webjars" % "webjars-play_2.12" % "2.6.0-M1",
    "org.webjars" % "bootstrap" % "3.3.7-1",
    "org.webjars" % "datatables" % "1.10.15",
    "org.webjars" % "jquery" % "3.2.1",
    "org.tpolecat" % "doobie-core-cats_2.12" % "0.4.1",
    "org.tpolecat" % "doobie-hikari-cats_2.12" % "0.4.1",
    "org.tpolecat" % "doobie-postgres-cats_2.12" % "0.4.1",
    "org.typelevel" % "cats-core_2.12" % "0.9.0",
    "org.scalatestplus.play" %% "scalatestplus-play" % "3.0.0-M3" % Test,
    "com.softwaremill.macwire" %% "macros" % "2.3.0" % "provided"
  ),
  dependencyOverrides ++= Seq(
    "org.webjars" % "bootstrap" % "3.3.7-1",
    "org.webjars" % "jquery" % "3.2.1"
  ),
  resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
).
  enablePlugins(PlayScala, SbtWeb).
  aggregate(jsProjects.map(projectToRef): _*)


lazy val jsProjects = Seq(js)


lazy val js = (project in file("client")).settings(
  scalaVersion := scalaV,
  scalaJSUseMainModuleInitializer := true,
  autoCompilerPlugins := true,
  scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature"),
  libraryDependencies ++= Seq(
    "org.scala-js" % "scalajs-dom_sjs0.6_2.12" % "0.9.3",
    "org.webjars" % "jquery" % "3.2.1",
    "org.webjars" % "bootstrap" % "3.3.7-1",
    "be.doeraene" % "scalajs-jquery_sjs0.6_2.12" % "0.9.2"
//    "com.mediamath" %%% "scala-json" % "1.0"
  ),
  dependencyOverrides ++= Seq(
    "org.webjars" % "bootstrap" % "3.3.7-1",
    "org.webjars" % "jquery" % "3.2.1"
  ),
  resolvers += "mmreleases" at "https://artifactory.mediamath.com/artifactory/libs-release-global",
  resolvers += Resolver.sonatypeRepo("releases"),
  addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)
).enablePlugins(ScalaJSPlugin, ScalaJSWeb)


